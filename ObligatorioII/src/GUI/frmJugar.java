/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import LOGICA.Equipo;
import LOGICA.Jugador;
import LOGICA.Pronostico;
import LOGICA.Sistema;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Diego
 */
public class frmJugar extends javax.swing.JFrame {
     Sistema sistema;
     Jugador jugador;
     int contador=1;
     
     
        ImageIcon iconoRusia=new ImageIcon(this.getClass().getResource("/IMAGENES/rusia.png"));
        ImageIcon iconoArabia=new ImageIcon(this.getClass().getResource("/IMAGENES/arabia saud.png"));
        ImageIcon iconoUruguay=new ImageIcon(this.getClass().getResource("/IMAGENES/uruguay.png"));
        ImageIcon iconoEgipto=new ImageIcon(this.getClass().getResource("/IMAGENES/egipto.png"));
     
        ImageIcon iconoPortugal=new ImageIcon(this.getClass().getResource("/IMAGENES/portugal.png"));
        ImageIcon iconoEspania=new ImageIcon(this.getClass().getResource("/IMAGENES/espania.png"));
        ImageIcon iconoMarruecos=new ImageIcon(this.getClass().getResource("/IMAGENES/marruecos.png"));
        ImageIcon iconoIran=new ImageIcon(this.getClass().getResource("/IMAGENES/iran.png"));
        
        ImageIcon iconoFrancia=new ImageIcon(this.getClass().getResource("/IMAGENES/francia.png"));
        ImageIcon iconoAustralia=new ImageIcon(this.getClass().getResource("/IMAGENES/australia.png"));
        ImageIcon iconoPeru=new ImageIcon(this.getClass().getResource("/IMAGENES/peru.png"));
        ImageIcon iconoDinamarca=new ImageIcon(this.getClass().getResource("/IMAGENES/dinamarca.png"));
        
        
        ImageIcon iconoArgentina=new ImageIcon(this.getClass().getResource("/IMAGENES/argentina.png"));
        ImageIcon iconoIslandia=new ImageIcon(this.getClass().getResource("/IMAGENES/islandia.png"));
        ImageIcon iconoCroacia=new ImageIcon(this.getClass().getResource("/IMAGENES/croacia.png"));
        ImageIcon iconoNigeria=new ImageIcon(this.getClass().getResource("/IMAGENES/nigeria.png"));
        
        
        ImageIcon iconoBrasil=new ImageIcon(this.getClass().getResource("/IMAGENES/brasil.png"));
        ImageIcon iconoSuiza=new ImageIcon(this.getClass().getResource("/IMAGENES/suiza.png"));
        ImageIcon iconoCostaRica=new ImageIcon(this.getClass().getResource("/IMAGENES/costa rica.png"));
        ImageIcon iconoSerbia=new ImageIcon(this.getClass().getResource("/IMAGENES/serbia.png"));
        
        
        ImageIcon iconoAlemania=new ImageIcon(this.getClass().getResource("/IMAGENES/alemania.png"));
        ImageIcon iconoMexico=new ImageIcon(this.getClass().getResource("/IMAGENES/mexico.png"));
        ImageIcon iconoSuecia=new ImageIcon(this.getClass().getResource("/IMAGENES/suecia.png"));
        ImageIcon iconoCoreadelSur=new ImageIcon(this.getClass().getResource("/IMAGENES/corea del sur.png"));
        
        
        ImageIcon iconoBelgica=new ImageIcon(this.getClass().getResource("/IMAGENES/belgica.png"));
        ImageIcon iconoPanama=new ImageIcon(this.getClass().getResource("/IMAGENES/panama.png"));
        ImageIcon iconoTunez=new ImageIcon(this.getClass().getResource("/IMAGENES/tunez.png"));
        ImageIcon iconoInglaterra=new ImageIcon(this.getClass().getResource("/IMAGENES/inglaterra.png"));
        
        
        ImageIcon iconoPolonia=new ImageIcon(this.getClass().getResource("/IMAGENES/polonia.png"));
        ImageIcon iconoSenegal=new ImageIcon(this.getClass().getResource("/IMAGENES/senegal.png"));
        ImageIcon iconoColombia=new ImageIcon(this.getClass().getResource("/IMAGENES/colombia.png"));
        ImageIcon iconoJapon=new ImageIcon(this.getClass().getResource("/IMAGENES/japon.png"));
        
        
        
        
        
        
        
     
    /**
     * Creates new form frmJugar
     */
    public frmJugar(Sistema sis, Jugador unJ) {
        
        sistema = sis;
        jugador = unJ;
        int resp=0;
        
        setIconImage(new ImageIcon(getClass().getResource("/IMAGENES/copa.jpg")).getImage());
       
       
       ((JPanel)getContentPane()).setOpaque(false);
        ImageIcon uno=new ImageIcon(this.getClass().getResource("/IMAGENES/fondo.jpg"));
        JLabel fondo= new JLabel();
        fondo.setIcon(uno);
        
        fondo.setBounds(0,0,uno.getIconWidth(),uno.getIconHeight());

        getLayeredPane().add(fondo,JLayeredPane.FRAME_CONTENT_LAYER);
        
         JTextField jP1E1Goles = new JTextField();
         JTextField jP1E2Goles = new JTextField();
         
         
        
        
        
        nombreP1 = new JLabel();
        nombreP2 = new JLabel();
        nombreP3 = new JLabel();
        nombreP4 = new JLabel();
        nombreP1 = new JLabel();
        nombreP6 = new JLabel();
        nombreP7 = new JLabel();
        nombreP8 = new JLabel();
        nombreP9 = new JLabel();
        nombreP10 = new JLabel();
        nombreP11 = new JLabel();
        nombreP12 = new JLabel();
        banderaP1 = new JLabel();
        estadioP1 = new JLabel();
        
        
        
        JLabel jlabel1 = new JLabel();
        String alias = jugador.getAlias();
        initComponents();
       
        jLabelJ.setText(unJ.getAlias());
        
        banderaP1.setIcon(iconoRusia);
        banderaP2.setIcon(iconoArabia);
        banderaP3.setIcon(iconoEgipto);
        banderaP4.setIcon(iconoUruguay);
        banderaP5.setIcon(iconoRusia);
        banderaP6.setIcon(iconoEgipto);
        banderaP7.setIcon(iconoUruguay);
        banderaP8.setIcon(iconoArabia);
        banderaP9.setIcon(iconoUruguay);
        banderaP10.setIcon(iconoRusia);
        banderaP11.setIcon(iconoArabia);
        banderaP12.setIcon(iconoEgipto);
        
        
       
        
         String equipo11 =  sistema.getListaPartidos()[1] .getEquipo1().getNombre();
         String equipo12 =  sistema.getListaPartidos()[1] .getEquipo2().getNombre();
         String estadio1 = sistema.getListaPartidos()[1] .getEstadio();
         String fecha1 = sistema.getListaPartidos()[1] .getInfo();
         nombreP1.setText(equipo11);
         nombreP2.setText(equipo12);
         estadioP1.setText(fecha1 + " - " + estadio1);
         
         String equipo21 =  sistema.getListaPartidos()[2] .getEquipo1().getNombre();
         String equipo22 =  sistema.getListaPartidos()[2] .getEquipo2().getNombre();
         String estadio2 = sistema.getListaPartidos()[2] .getEstadio();
         String fecha2 = sistema.getListaPartidos()[2] .getInfo();
         nombreP3.setText(equipo21);
         nombreP4.setText(equipo22);
         estadioP2.setText(fecha2 + " - " + estadio2);
         
         String equipo31 =  sistema.getListaPartidos()[3] .getEquipo1().getNombre();
         String equipo32 =  sistema.getListaPartidos()[3] .getEquipo2().getNombre();
         String estadio3 = sistema.getListaPartidos()[3] .getEstadio();
         String fecha3 = sistema.getListaPartidos()[3] .getInfo();
         nombreP5.setText(equipo31);
         nombreP6.setText(equipo32);
         estadioP3.setText(fecha3 + " - " + estadio3);
         
         String equipo41 =  sistema.getListaPartidos()[4] .getEquipo1().getNombre();
         String equipo42 =  sistema.getListaPartidos()[4] .getEquipo2().getNombre();
         String estadio4 = sistema.getListaPartidos()[4] .getEstadio();
         String fecha4 = sistema.getListaPartidos()[4] .getInfo();
         nombreP7.setText(equipo41);
         nombreP8.setText(equipo42);
         estadioP4.setText(fecha4 + " - " + estadio4);
         
         String equipo51 =  sistema.getListaPartidos()[5] .getEquipo1().getNombre();
         String equipo52 =  sistema.getListaPartidos()[5] .getEquipo2().getNombre();
         String estadio5 = sistema.getListaPartidos()[5] .getEstadio();
         String fecha5 = sistema.getListaPartidos()[5] .getInfo();
         nombreP9.setText(equipo51);
         nombreP10.setText(equipo52);
         estadioP5.setText(fecha5 + " - " + estadio5);
         
         String equipo61 =  sistema.getListaPartidos()[6] .getEquipo1().getNombre();
         String equipo62 =  sistema.getListaPartidos()[6] .getEquipo2().getNombre();
         String estadio6 = sistema.getListaPartidos()[6] .getEstadio();
         String fecha6 = sistema.getListaPartidos()[6] .getInfo();
         nombreP11.setText(equipo61);
         nombreP12.setText(equipo62);
         estadioP6.setText(fecha6 + " - " + estadio6);
          
         String aliasLista="";
        DefaultListModel listaModelo = new DefaultListModel();
        for (int i= 0 ;i < sistema.getListaJugadores().size(); i++){
            aliasLista = sistema.getListaJugadores().get(i).toString();
            listaModelo.addElement(aliasLista);
           }
         
         
         
        jListaRanking.setModel(listaModelo);
         
        setLocationRelativeTo(null);
        setVisible(true);
        
        File archivo = null;
      FileReader fr = null;
      BufferedReader br = null;
      String[] dato = null;
     // Partido p1 = new Partido();
      
      
      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         
        
       
       BufferedReader reader = new BufferedReader(new InputStreamReader(new DataInputStream(getClass().getResource("/ARCHIVOS/resultados.txt").openStream())));
     

         // Lectura del fichero
         String linea = "";
         
         
         int contador=1;
         
         
          while((linea=reader.readLine())!=null){
          //recorro archivo de resultados 1era fase.
            Equipo e1 = new Equipo();
            Equipo e2 = new Equipo();
            dato = linea.split("#");
         //los cargo en un string
         
         if(contador < 49){
         String golesEquipo1=dato[1];
         String golesEquipo2=dato[3];
         String equipo1=dato[0];
         String equipo2=dato[2];
         
         e1 = sistema.devuelvoEquipo(equipo1);
         e2 = sistema.devuelvoEquipo(equipo2);
         //los convierto a int.
         int golEquipo1 = Integer.parseInt(golesEquipo1);
         int golEquipo2 = Integer.parseInt(golesEquipo2);
         
         sistema.getListaPartidos()[contador].setGolEquipo1(golEquipo1);
         sistema.getListaPartidos()[contador].setGolEquipo2(golEquipo2);
         sistema.resultado(e1, e2, golEquipo1, golEquipo2);
         }
         if(contador > 48){
             
         //los cargo en un string
         String golesEquipo1=dato[0];
         String golesEquipo2=dato[1];
         //los convierto a int.
         int golEquipo1 = Integer.parseInt(golesEquipo1);
         int golEquipo2 = Integer.parseInt(golesEquipo2);
         
         sistema.getListaPartidos()[contador].setGolEquipo1(golEquipo1);
         sistema.getListaPartidos()[contador].setGolEquipo2(golEquipo2);
         sistema.resultado(e1, e2, golEquipo1, golEquipo2);
             
         }
         
         
         
         contador++;   
      }
         
         
      }
      catch(Exception e){
         e.printStackTrace();
      }finally{
         // En el finally cerramos el fichero, para asegurarnos
         // que se cierra tanto si todo va bien como si salta 
         // una excepcion.
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
   
      
      
      
       
            
       
            
          
                
                if(unJ.getListaPronostico()[contador] != null){
                    jP1E1Goles.setText(String.valueOf(unJ.getListaPronostico()[contador].getGolesEquipo1()));
                    jP1E2Goles.setText(String.valueOf(unJ.getListaPronostico()[contador].getGolesEquipo2()));
                }
               if(unJ.getListaPronostico()[contador + 1] != null){
                    jP2E1Goles.setText(String.valueOf(unJ.getListaPronostico()[contador + 1].getGolesEquipo1()));
                    jP2E2Goles.setText(String.valueOf(unJ.getListaPronostico()[contador + 1].getGolesEquipo2()));
               }
               if(unJ.getListaPronostico()[contador + 2] != null){
                   jP3E1Goles.setText(String.valueOf(unJ.getListaPronostico()[contador + 2].getGolesEquipo1()));
                   jP3E2Goles.setText(String.valueOf(unJ.getListaPronostico()[contador + 2].getGolesEquipo2()));
               }
               if(unJ.getListaPronostico()[contador + 3] != null){
                jP4E1Goles.setText(String.valueOf(unJ.getListaPronostico()[contador + 3].getGolesEquipo1()));
                jP4E2Goles.setText(String.valueOf(unJ.getListaPronostico()[contador + 3].getGolesEquipo2()));
               }
              if(unJ.getListaPronostico()[contador + 4] != null){
              jP5E1Goles.setText(String.valueOf(unJ.getListaPronostico()[contador + 4].getGolesEquipo1()));
              jP5E2Goles.setText(String.valueOf(unJ.getListaPronostico()[contador + 4].getGolesEquipo2()));
              }
              if(unJ.getListaPronostico()[contador + 5] != null){
              jP6E1Goles.setText(String.valueOf(unJ.getListaPronostico()[contador + 5].getGolesEquipo1()));
              jP6E2Goles.setText(String.valueOf(unJ.getListaPronostico()[contador + 5].getGolesEquipo2()));   
                  
              }
              
              
             
              
         
        
        
    

        // TODO add your handling code here:
       
    }

    private frmJugar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jBotonComienzo = new javax.swing.JButton();
        jBotonSiguiente = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListaRanking = new javax.swing.JList();
        jLabelJugador = new javax.swing.JLabel();
        jLabelPuntos = new javax.swing.JLabel();
        jPanelFase = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jP1E1Goles = new javax.swing.JTextField();
        jP1E2Goles = new javax.swing.JTextField();
        nombreP2 = new javax.swing.JLabel();
        nombreP1 = new javax.swing.JLabel();
        banderaP1 = new javax.swing.JLabel();
        banderaP2 = new javax.swing.JLabel();
        estadioP1 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jP2E1Goles = new javax.swing.JTextField();
        jP2E2Goles = new javax.swing.JTextField();
        nombreP4 = new javax.swing.JLabel();
        nombreP3 = new javax.swing.JLabel();
        banderaP3 = new javax.swing.JLabel();
        banderaP4 = new javax.swing.JLabel();
        estadioP2 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jP4E1Goles = new javax.swing.JTextField();
        jP4E2Goles = new javax.swing.JTextField();
        nombreP8 = new javax.swing.JLabel();
        nombreP7 = new javax.swing.JLabel();
        banderaP7 = new javax.swing.JLabel();
        banderaP8 = new javax.swing.JLabel();
        estadioP4 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jP3E1Goles = new javax.swing.JTextField();
        jP3E2Goles = new javax.swing.JTextField();
        nombreP6 = new javax.swing.JLabel();
        nombreP5 = new javax.swing.JLabel();
        banderaP5 = new javax.swing.JLabel();
        banderaP6 = new javax.swing.JLabel();
        estadioP3 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jP5E1Goles = new javax.swing.JTextField();
        jP5E2Goles = new javax.swing.JTextField();
        nombreP10 = new javax.swing.JLabel();
        nombreP9 = new javax.swing.JLabel();
        banderaP9 = new javax.swing.JLabel();
        banderaP10 = new javax.swing.JLabel();
        estadioP5 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jP6E1Goles = new javax.swing.JTextField();
        jP6E2Goles = new javax.swing.JTextField();
        nombreP12 = new javax.swing.JLabel();
        nombreP11 = new javax.swing.JLabel();
        banderaP11 = new javax.swing.JLabel();
        banderaP12 = new javax.swing.JLabel();
        estadioP6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabelJ = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jBotonSig1 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("JUEGO PENCA COPA DEL MUNDO RUSIA 2018");
        setPreferredSize(new java.awt.Dimension(730, 650));

        jBotonComienzo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGENES/flechaizq.png"))); // NOI18N
        jBotonComienzo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonComienzoActionPerformed(evt);
            }
        });

        jBotonSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGENES/flechader.png"))); // NOI18N
        jBotonSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonSiguienteActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(180, 36, 36));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jListaRanking.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(jListaRanking);

        jLabelJugador.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabelJugador.setText("JUGADOR");

        jLabelPuntos.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabelPuntos.setText("PUNTOS");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabelJugador)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabelPuntos)
                .addGap(40, 40, 40))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelJugador)
                    .addComponent(jLabelPuntos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanelFase.setBackground(new java.awt.Color(180, 36, 36));
        jPanelFase.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP1.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(nombreP1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(banderaP1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP1E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP1E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(banderaP2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nombreP2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP1, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(banderaP2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jP1E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jP1E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(banderaP1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP1, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP2.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(nombreP3, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(banderaP3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP2E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP2E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(banderaP4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nombreP4, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP2, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(banderaP4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jP2E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jP2E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(banderaP3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP2, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP4.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(nombreP7, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(banderaP7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP4E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP4E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(banderaP8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nombreP8, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP4, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(banderaP8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jP4E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jP4E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(banderaP7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP4, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP3.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(nombreP5, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(banderaP5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP3E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP3E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(banderaP6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nombreP6, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP3, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(banderaP6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jP3E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jP3E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(banderaP5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP3, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP5.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(nombreP9, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(banderaP9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP5E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP5E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(banderaP10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nombreP10, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP5, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(banderaP10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jP5E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jP5E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(banderaP9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP5, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP6.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(nombreP11, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(banderaP11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP6E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jP6E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(banderaP12, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nombreP12, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP6, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(banderaP12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jP6E1Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jP6E2Goles, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(banderaP11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP6, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanelFaseLayout = new javax.swing.GroupLayout(jPanelFase);
        jPanelFase.setLayout(jPanelFaseLayout);
        jPanelFaseLayout.setHorizontalGroup(
            jPanelFaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelFaseLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanelFaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelFaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanelFaseLayout.setVerticalGroup(
            jPanelFaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFaseLayout.createSequentialGroup()
                .addContainerGap(25, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(180, 36, 36));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("JUGADOR"));
        jPanel2.setForeground(new java.awt.Color(255, 153, 51));

        jLabelJ.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabelJ, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabelJ, javax.swing.GroupLayout.DEFAULT_SIZE, 7, Short.MAX_VALUE)
        );

        jPanel4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jBotonSig1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGENES/sig.png"))); // NOI18N
        jBotonSig1.setMaximumSize(new java.awt.Dimension(50, 50));
        jBotonSig1.setMinimumSize(new java.awt.Dimension(50, 50));
        jBotonSig1.setPreferredSize(new java.awt.Dimension(50, 50));
        jBotonSig1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonSig1ActionPerformed(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGENES/salir.png"))); // NOI18N
        jButton6.setPreferredSize(new java.awt.Dimension(80, 60));
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGENES/guardar.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBotonSig1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jButton6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
            .addComponent(jBotonSig1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jBotonComienzo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(78, 78, 78)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jBotonSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanelFase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(211, 211, 211))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanelFase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jBotonComienzo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jBotonSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(67, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        DefaultListModel listaModelo = new DefaultListModel();


        // TODO add your handling code here:
        int cont = 0;
        
        Pronostico p = new Pronostico();
        
        
            
        if(!jP1E1Goles.getText().trim().isEmpty() && !jP1E2Goles.getText().trim().isEmpty() && sistema.soloNumeros(jP1E1Goles.getText()) && sistema.soloNumeros(jP1E2Goles.getText())){
            
            if(sistema.fechaValida(contador)){
                
            p.setGolesEquipo1(Integer.parseInt(jP1E1Goles.getText()));
            p.setGolesEquipo2(Integer.parseInt(jP1E2Goles.getText()));
            jugador.agregarPronostico(p, contador);
            sistema.actualizaRanking(contador);
            jP1E1Goles.setEnabled(false);
            jP1E2Goles.setEnabled(false);
            
            
            
            
            String aliasLista="";
            for (int i= 0 ;i < sistema.getListaJugadores().size(); i++){
            aliasLista = sistema.getListaJugadores().get(i).toString();
            listaModelo.addElement(aliasLista);
           }
            jListaRanking.setModel(listaModelo);
            cont++;
            }
            else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
            }
            
        }
        else{
            
            //JOptionPane.showMessageDialog(this, "Pronóstico incorrecto, ingrese nuevamente");
            
        }
        
        
       
        
        
            
        
        if(!jP2E1Goles.getText().trim().isEmpty() && !jP2E2Goles.getText().trim().isEmpty() && sistema.soloNumeros(jP2E1Goles.getText()) && sistema.soloNumeros(jP2E2Goles.getText())){
        
                if(sistema.fechaValida(contador + 1)){
            
                 p.setGolesEquipo1(Integer.parseInt(jP2E1Goles.getText()));
                 p.setGolesEquipo2(Integer.parseInt(jP2E2Goles.getText()));
                jugador.agregarPronostico(p, contador + 1);
                sistema.actualizaRanking(contador);
                jP2E1Goles.setEnabled(false);
                jP2E2Goles.setEnabled(false);
                String aliasLista="";
            for (int i= 0 ;i < sistema.getListaJugadores().size(); i++){
            aliasLista = sistema.getListaJugadores().get(i).toString();
            listaModelo.addElement(aliasLista);
           }
            jListaRanking.setModel(listaModelo);
               
                cont++;
                }
                
                else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
                }
                
        }
        else{
            
            //JOptionPane.showMessageDialog(this, "Pronóstico incorrecto, ingrese nuevamente");
            
        }
        
         
        
        
        if(!jP3E1Goles.getText().trim().isEmpty() && !jP3E2Goles.getText().trim().isEmpty() && sistema.soloNumeros(jP3E1Goles.getText()) && sistema.soloNumeros(jP3E2Goles.getText())){
        
           if(sistema.fechaValida(contador + 2)){ 
            
               p.setGolesEquipo1(Integer.parseInt(jP3E1Goles.getText()));
                p.setGolesEquipo2(Integer.parseInt(jP3E2Goles.getText()));
                jugador.agregarPronostico(p, contador + 2);
                sistema.actualizaRanking(contador);
                jP3E1Goles.setEnabled(false);
                jP3E2Goles.setEnabled(false);
                String aliasLista="";
            for (int i= 0 ;i < sistema.getListaJugadores().size(); i++){
            aliasLista = sistema.getListaJugadores().get(i).toString();
            listaModelo.addElement(aliasLista);
           }
            jListaRanking.setModel(listaModelo);
                
                cont++;
                }
           
            else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
                }
        }
        else{
            
            //JOptionPane.showMessageDialog(this, "Pronóstico incorrecto, ingrese nuevamente");
            
        }
        
       
        
        
        if(!jP4E1Goles.getText().trim().isEmpty() && !jP4E2Goles.getText().trim().isEmpty() && sistema.soloNumeros(jP4E1Goles.getText()) && sistema.soloNumeros(jP4E2Goles.getText())){
        
            if(sistema.fechaValida(contador + 3)){
            p.setGolesEquipo1(Integer.parseInt(jP4E1Goles.getText()));
            p.setGolesEquipo2(Integer.parseInt(jP4E2Goles.getText()));
            jugador.agregarPronostico(p, contador + 3);
            sistema.actualizaRanking(contador);
            jP4E1Goles.setEnabled(false);
            jP4E2Goles.setEnabled(false);
            String aliasLista="";
            for (int i= 0 ;i < sistema.getListaJugadores().size(); i++){
            aliasLista = sistema.getListaJugadores().get(i).toString();
            listaModelo.addElement(aliasLista);
           }
            jListaRanking.setModel(listaModelo);
             cont++;
            }
             else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
            }
            
        }   
        else{
            
            //JOptionPane.showMessageDialog(this, "Pronóstico incorrecto, ingrese nuevamente");
           
        }
        
       
        
        if(!jP5E1Goles.getText().trim().isEmpty() && !jP5E2Goles.getText().trim().isEmpty() && sistema.soloNumeros(jP5E1Goles.getText()) && sistema.soloNumeros(jP5E2Goles.getText())){
        
            if(sistema.fechaValida(contador + 4)){  
            p.setGolesEquipo1(Integer.parseInt(jP5E1Goles.getText()));
            p.setGolesEquipo2(Integer.parseInt(jP5E2Goles.getText()));
            jugador.agregarPronostico(p, contador + 4);
            sistema.actualizaRanking(contador);
            jP5E1Goles.setEnabled(false);
            jP5E2Goles.setEnabled(false);
            String aliasLista="";
            for (int i= 0 ;i < sistema.getListaJugadores().size(); i++){
            aliasLista = sistema.getListaJugadores().get(i).toString();
            listaModelo.addElement(aliasLista);
           }
            jListaRanking.setModel(listaModelo);
            cont++;
            }
            else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
            }
        }
        else{
            
            //JOptionPane.showMessageDialog(this, "Pronóstico incorrecto, ingrese nuevamente");
            
        }
        
        
        
        if(!jP6E1Goles.getText().trim().isEmpty() && !jP6E2Goles.getText().trim().isEmpty() && sistema.soloNumeros(jP6E1Goles.getText()) && sistema.soloNumeros(jP6E2Goles.getText())){
       
            if(sistema.fechaValida(contador + 5)){
            p.setGolesEquipo1(Integer.parseInt(jP6E1Goles.getText()));
            p.setGolesEquipo2(Integer.parseInt(jP6E2Goles.getText()));
            jugador.agregarPronostico(p, contador + 5);
            sistema.actualizaRanking(contador);
            jP6E1Goles.setEnabled(false);
            jP6E2Goles.setEnabled(false);
            String aliasLista="";
            for (int i= 0 ;i < sistema.getListaJugadores().size(); i++){
            aliasLista = sistema.getListaJugadores().get(i).toString();
            listaModelo.addElement(aliasLista);
           }
            jListaRanking.setModel(listaModelo);
            cont++;
            }
            else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
            }
        }
        else{
            
            //JOptionPane.showMessageDialog(this, "Pronóstico incorrecto, ingrese nuevamente");
            
        }
        
        
        if(!jP1E1Goles.getText().trim().isEmpty() && !jP1E2Goles.getText().trim().isEmpty() ||
                !jP2E1Goles.getText().trim().isEmpty() && !jP2E2Goles.getText().trim().isEmpty() || 
                !jP3E1Goles.getText().trim().isEmpty() || !jP3E2Goles.getText().trim().isEmpty() ||
                !jP4E1Goles.getText().trim().isEmpty() && !jP4E2Goles.getText().trim().isEmpty() ||
                !jP5E1Goles.getText().trim().isEmpty() && !jP5E2Goles.getText().trim().isEmpty() || 
                !jP6E1Goles.getText().trim().isEmpty() || !jP6E2Goles.getText().trim().isEmpty()){
            if(cont <6 && cont > 0){
                 JOptionPane.showMessageDialog(this, "Se agrego pronóstico del Grupo satisfactoriamente.");
            }
            else{
                JOptionPane.showMessageDialog(this, "Pronóstico incorrecto, ingrese nuevamente.");
            }
       
        
        }
         else{
                JOptionPane.showMessageDialog(this, "Debe ingresar al menos 1 Pronóstico.");
            }
        
        jP1E1Goles.setText("");
        jP1E2Goles.setText("");
        jP2E1Goles.setText("");
        jP2E2Goles.setText("");
        jP3E1Goles.setText("");
        jP3E2Goles.setText("");
        jP4E1Goles.setText("");
        jP4E2Goles.setText("");
        jP5E1Goles.setText("");
        jP5E2Goles.setText("");
        jP6E1Goles.setText("");
        jP6E2Goles.setText("");
        
        
        
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jBotonSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonSiguienteActionPerformed

        
     
        
        
        
        
        
        
        
        contador = contador+6;
        jBotonComienzo.setEnabled(true);
        jP1E1Goles.setEnabled(true);
        jP1E2Goles.setEnabled(true);
        jP2E1Goles.setEnabled(true);
        jP2E2Goles.setEnabled(true);
        jP3E1Goles.setEnabled(true);
        jP3E2Goles.setEnabled(true);
        jP4E1Goles.setEnabled(true);
        jP4E2Goles.setEnabled(true);
        jP5E1Goles.setEnabled(true);
        jP5E2Goles.setEnabled(true);
        jP6E1Goles.setEnabled(true);
        jP6E2Goles.setEnabled(true);
        
           if(jugador.getListaPronostico()[contador] != null){
                    jP1E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador].getGolesEquipo1()));
                    jP1E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador].getGolesEquipo2()));
                }
               if(jugador.getListaPronostico()[contador + 1] != null){
                    jP2E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 1].getGolesEquipo1()));
                    jP2E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 1].getGolesEquipo2()));
               }
               if(jugador.getListaPronostico()[contador + 2] != null){
                   jP3E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 2].getGolesEquipo1()));
                   jP3E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 2].getGolesEquipo2()));
               }
               if(jugador.getListaPronostico()[contador + 3] != null){
                jP4E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 3].getGolesEquipo1()));
                jP4E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 3].getGolesEquipo2()));
               }
              if(jugador.getListaPronostico()[contador + 4] != null){
              jP5E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 4].getGolesEquipo1()));
              jP5E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 4].getGolesEquipo2()));
              }
              if(jugador.getListaPronostico()[contador + 5] != null){
              jP6E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 5].getGolesEquipo1()));
              jP6E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 5].getGolesEquipo2()));   
                  
              }
        
        
        
        if(contador ==7){
        
        
        banderaP1.setIcon(iconoMarruecos);
        banderaP2.setIcon(iconoIran);
        banderaP3.setIcon(iconoPortugal);
        banderaP4.setIcon(iconoEspania);
        banderaP5.setIcon(iconoPortugal);
        banderaP6.setIcon(iconoMarruecos);
        banderaP7.setIcon(iconoIran);
        banderaP8.setIcon(iconoEspania);
        banderaP9.setIcon(iconoIran);
        banderaP10.setIcon(iconoPortugal);
        banderaP11.setIcon(iconoEspania);
        banderaP12.setIcon(iconoMarruecos);
        }
        else if(contador == 13){
            
        banderaP1.setIcon(iconoFrancia);
        banderaP2.setIcon(iconoAustralia);
        banderaP3.setIcon(iconoPeru);
        banderaP4.setIcon(iconoDinamarca);
        banderaP5.setIcon(iconoFrancia);
        banderaP6.setIcon(iconoPeru);
        banderaP7.setIcon(iconoDinamarca);
        banderaP8.setIcon(iconoAustralia);
        banderaP9.setIcon(iconoDinamarca);
        banderaP10.setIcon(iconoFrancia);
        banderaP11.setIcon(iconoAustralia);
        banderaP12.setIcon(iconoPeru);
            
        }
        else if(contador == 19){
            
        banderaP1.setIcon(iconoArgentina);
        banderaP2.setIcon(iconoIslandia);
        banderaP3.setIcon(iconoCroacia);
        banderaP4.setIcon(iconoNigeria);
        banderaP5.setIcon(iconoArgentina);
        banderaP6.setIcon(iconoCroacia);
        banderaP7.setIcon(iconoNigeria);
        banderaP8.setIcon(iconoIslandia);
        banderaP9.setIcon(iconoNigeria);
        banderaP10.setIcon(iconoArgentina);
        banderaP11.setIcon(iconoIslandia);
        banderaP12.setIcon(iconoCroacia);
            
        }
        else if(contador == 25){
            
        banderaP1.setIcon(iconoCostaRica);
        banderaP2.setIcon(iconoSerbia);
        banderaP3.setIcon(iconoBrasil);
        banderaP4.setIcon(iconoSuiza);
        banderaP5.setIcon(iconoBrasil);
        banderaP6.setIcon(iconoCostaRica);
        banderaP7.setIcon(iconoSerbia);
        banderaP8.setIcon(iconoSuiza);
        banderaP9.setIcon(iconoSerbia);
        banderaP10.setIcon(iconoBrasil);
        banderaP11.setIcon(iconoSuiza);
        banderaP12.setIcon(iconoCostaRica);   
            
        }
        else if(contador == 31){
            
        banderaP1.setIcon(iconoAlemania);
        banderaP2.setIcon(iconoMexico);
        banderaP3.setIcon(iconoSuecia);
        banderaP4.setIcon(iconoCoreadelSur);
        banderaP5.setIcon(iconoAlemania);
        banderaP6.setIcon(iconoSuecia);
        banderaP7.setIcon(iconoCoreadelSur);
        banderaP8.setIcon(iconoMexico);
        banderaP9.setIcon(iconoCoreadelSur);
        banderaP10.setIcon(iconoAlemania);
        banderaP11.setIcon(iconoMexico);
        banderaP12.setIcon(iconoSuecia);   
            
            
        }
        
        else if(contador == 37){
            
        banderaP1.setIcon(iconoBelgica);
        banderaP2.setIcon(iconoPanama);
        banderaP3.setIcon(iconoTunez);
        banderaP4.setIcon(iconoInglaterra);
        banderaP5.setIcon(iconoBelgica);
        banderaP6.setIcon(iconoTunez);
        banderaP7.setIcon(iconoInglaterra);
        banderaP8.setIcon(iconoPanama);
        banderaP9.setIcon(iconoInglaterra);
        banderaP10.setIcon(iconoBelgica);
        banderaP11.setIcon(iconoPanama);
        banderaP12.setIcon(iconoTunez);    
            
            
        }
        
        else if(contador == 43){
            
        banderaP1.setIcon(iconoPolonia);
        banderaP2.setIcon(iconoSenegal);
        banderaP3.setIcon(iconoColombia);
        banderaP4.setIcon(iconoJapon);
        banderaP5.setIcon(iconoJapon);
        banderaP6.setIcon(iconoSenegal);
        banderaP7.setIcon(iconoPolonia);
        banderaP8.setIcon(iconoColombia);
        banderaP9.setIcon(iconoJapon);
        banderaP10.setIcon(iconoPolonia);
        banderaP11.setIcon(iconoSenegal);
        banderaP12.setIcon(iconoColombia);    
            
        }

        
        if(contador <49){
            
            
         String equipo11 =  sistema.getListaPartidos()[contador] .getEquipo1().getNombre();
         String equipo12 =  sistema.getListaPartidos()[contador] .getEquipo2().getNombre();
         String estadio1 = sistema.getListaPartidos()[contador] .getEstadio();
         String fecha1 = sistema.getListaPartidos()[contador] .getInfo();
         nombreP1.setText(equipo11);
         nombreP2.setText(equipo12);
         estadioP1.setText(fecha1 + " - " + estadio1);
         
         String equipo21 =  sistema.getListaPartidos()[contador + 1] .getEquipo1().getNombre();
         String equipo22 =  sistema.getListaPartidos()[contador + 1] .getEquipo2().getNombre();
         String estadio2 = sistema.getListaPartidos()[contador + 1] .getEstadio();
         String fecha2 = sistema.getListaPartidos()[contador + 1] .getInfo();
         nombreP3.setText(equipo21);
         nombreP4.setText(equipo22);
         estadioP2.setText(fecha2 + " - " + estadio2);
         
         String equipo31 =  sistema.getListaPartidos()[contador + 2] .getEquipo1().getNombre();
         String equipo32 =  sistema.getListaPartidos()[contador + 2] .getEquipo2().getNombre();
         String estadio3 = sistema.getListaPartidos()[contador + 2] .getEstadio();
         String fecha3 = sistema.getListaPartidos()[contador + 2] .getInfo();
         nombreP5.setText(equipo31);
         nombreP6.setText(equipo32);
         estadioP3.setText(fecha3 + " - " + estadio3);
         
         String equipo41 =  sistema.getListaPartidos()[contador + 3] .getEquipo1().getNombre();
         String equipo42 =  sistema.getListaPartidos()[contador + 3] .getEquipo2().getNombre();
         String estadio4 = sistema.getListaPartidos()[contador + 3] .getEstadio();
         String fecha4 = sistema.getListaPartidos()[contador + 3] .getInfo();
         nombreP7.setText(equipo41);
         nombreP8.setText(equipo42);
         estadioP4.setText(fecha4 + " - " + estadio4);
         
         String equipo51 =  sistema.getListaPartidos()[contador + 4] .getEquipo1().getNombre();
         String equipo52 =  sistema.getListaPartidos()[contador + 4] .getEquipo2().getNombre();
         String estadio5 = sistema.getListaPartidos()[contador + 4] .getEstadio();
         String fecha5 = sistema.getListaPartidos()[contador + 4] .getInfo();
         nombreP9.setText(equipo51);
         nombreP10.setText(equipo52);
         estadioP5.setText(fecha5 + " - " + estadio5);
         
         String equipo61 =  sistema.getListaPartidos()[contador + 5] .getEquipo1().getNombre();
         String equipo62 =  sistema.getListaPartidos()[contador + 5] .getEquipo2().getNombre();
         String estadio6 = sistema.getListaPartidos()[contador + 5] .getEstadio();
         String fecha6 = sistema.getListaPartidos()[contador + 5] .getInfo();
         nombreP11.setText(equipo61);
         nombreP12.setText(equipo62);
         estadioP6.setText(fecha6 + " - " + estadio6);
            
        }
        else{
            
          
            contador = contador-6;
            jBotonSiguiente.setEnabled(false);
        }
        
      
        
        



        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonSiguienteActionPerformed

    private void jBotonComienzoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonComienzoActionPerformed
        // TODO add your handling code here:
        
        contador = contador-6;
        jBotonSiguiente.setEnabled(true);
        
        jP1E1Goles.setEnabled(true);
        jP1E2Goles.setEnabled(true);
        jP2E1Goles.setEnabled(true);
        jP2E2Goles.setEnabled(true);
        jP3E1Goles.setEnabled(true);
        jP3E2Goles.setEnabled(true);
        jP4E1Goles.setEnabled(true);
        jP4E2Goles.setEnabled(true);
        jP5E1Goles.setEnabled(true);
        jP5E2Goles.setEnabled(true);
        jP6E1Goles.setEnabled(true);
        jP6E2Goles.setEnabled(true);
        
           if(jugador.getListaPronostico()[contador] != null){
                    jP1E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador].getGolesEquipo1()));
                    jP1E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador].getGolesEquipo2()));
                }
               if(jugador.getListaPronostico()[contador + 1] != null){
                    jP2E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 1].getGolesEquipo1()));
                    jP2E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 1].getGolesEquipo2()));
               }
               if(jugador.getListaPronostico()[contador + 2] != null){
                   jP3E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 2].getGolesEquipo1()));
                   jP3E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 2].getGolesEquipo2()));
               }
               if(jugador.getListaPronostico()[contador + 3] != null){
                jP4E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 3].getGolesEquipo1()));
                jP4E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 3].getGolesEquipo2()));
               }
              if(jugador.getListaPronostico()[contador + 4] != null){
              jP5E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 4].getGolesEquipo1()));
              jP5E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 4].getGolesEquipo2()));
              }
              if(jugador.getListaPronostico()[contador + 5] != null){
              jP6E1Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 5].getGolesEquipo1()));
              jP6E2Goles.setText(String.valueOf(jugador.getListaPronostico()[contador + 5].getGolesEquipo2()));   
                  
              }
        
       if(contador ==1){
        
        
        banderaP1.setIcon(iconoRusia);
        banderaP2.setIcon(iconoArabia);
        banderaP3.setIcon(iconoEgipto);
        banderaP4.setIcon(iconoUruguay);
        banderaP5.setIcon(iconoRusia);
        banderaP6.setIcon(iconoEgipto);
        banderaP7.setIcon(iconoUruguay);
        banderaP8.setIcon(iconoArabia);
        banderaP9.setIcon(iconoUruguay);
        banderaP10.setIcon(iconoRusia);
        banderaP11.setIcon(iconoArabia);
        banderaP12.setIcon(iconoEgipto);
        
        }
        
        
        
       else if(contador ==7){
        
        
        banderaP1.setIcon(iconoMarruecos);
        banderaP2.setIcon(iconoIran);
        banderaP3.setIcon(iconoPortugal);
        banderaP4.setIcon(iconoEspania);
        banderaP5.setIcon(iconoPortugal);
        banderaP6.setIcon(iconoMarruecos);
        banderaP7.setIcon(iconoIran);
        banderaP8.setIcon(iconoEspania);
        banderaP9.setIcon(iconoIran);
        banderaP10.setIcon(iconoPortugal);
        banderaP11.setIcon(iconoEspania);
        banderaP12.setIcon(iconoMarruecos);
        }
        else if(contador == 13){
            
        banderaP1.setIcon(iconoFrancia);
        banderaP2.setIcon(iconoAustralia);
        banderaP3.setIcon(iconoPeru);
        banderaP4.setIcon(iconoDinamarca);
        banderaP5.setIcon(iconoFrancia);
        banderaP6.setIcon(iconoPeru);
        banderaP7.setIcon(iconoDinamarca);
        banderaP8.setIcon(iconoAustralia);
        banderaP9.setIcon(iconoDinamarca);
        banderaP10.setIcon(iconoFrancia);
        banderaP11.setIcon(iconoAustralia);
        banderaP12.setIcon(iconoPeru);
            
        }
        else if(contador == 19){
            
        banderaP1.setIcon(iconoArgentina);
        banderaP2.setIcon(iconoIslandia);
        banderaP3.setIcon(iconoCroacia);
        banderaP4.setIcon(iconoNigeria);
        banderaP5.setIcon(iconoArgentina);
        banderaP6.setIcon(iconoCroacia);
        banderaP7.setIcon(iconoNigeria);
        banderaP8.setIcon(iconoIslandia);
        banderaP9.setIcon(iconoNigeria);
        banderaP10.setIcon(iconoArgentina);
        banderaP11.setIcon(iconoIslandia);
        banderaP12.setIcon(iconoCroacia);
            
        }
        else if(contador == 25){
            
        banderaP1.setIcon(iconoCostaRica);
        banderaP2.setIcon(iconoSerbia);
        banderaP3.setIcon(iconoBrasil);
        banderaP4.setIcon(iconoSuiza);
        banderaP5.setIcon(iconoBrasil);
        banderaP6.setIcon(iconoCostaRica);
        banderaP7.setIcon(iconoSerbia);
        banderaP8.setIcon(iconoSuiza);
        banderaP9.setIcon(iconoSerbia);
        banderaP10.setIcon(iconoBrasil);
        banderaP11.setIcon(iconoSuiza);
        banderaP12.setIcon(iconoCostaRica);   
            
        }
        else if(contador == 31){
            
        banderaP1.setIcon(iconoAlemania);
        banderaP2.setIcon(iconoMexico);
        banderaP3.setIcon(iconoSuecia);
        banderaP4.setIcon(iconoCoreadelSur);
        banderaP5.setIcon(iconoAlemania);
        banderaP6.setIcon(iconoSuecia);
        banderaP7.setIcon(iconoCoreadelSur);
        banderaP8.setIcon(iconoMexico);
        banderaP9.setIcon(iconoCoreadelSur);
        banderaP10.setIcon(iconoAlemania);
        banderaP11.setIcon(iconoMexico);
        banderaP12.setIcon(iconoSuecia);   
            
            
        }
        
        else if(contador == 37){
            
        banderaP1.setIcon(iconoBelgica);
        banderaP2.setIcon(iconoPanama);
        banderaP3.setIcon(iconoTunez);
        banderaP4.setIcon(iconoInglaterra);
        banderaP5.setIcon(iconoBelgica);
        banderaP6.setIcon(iconoTunez);
        banderaP7.setIcon(iconoInglaterra);
        banderaP8.setIcon(iconoPanama);
        banderaP9.setIcon(iconoInglaterra);
        banderaP10.setIcon(iconoBelgica);
        banderaP11.setIcon(iconoPanama);
        banderaP12.setIcon(iconoTunez);    
            
            
        }
        
        else if(contador == 43){
            
        banderaP1.setIcon(iconoPolonia);
        banderaP2.setIcon(iconoSenegal);
        banderaP3.setIcon(iconoColombia);
        banderaP4.setIcon(iconoJapon);
        banderaP5.setIcon(iconoJapon);
        banderaP6.setIcon(iconoSenegal);
        banderaP7.setIcon(iconoPolonia);
        banderaP8.setIcon(iconoColombia);
        banderaP9.setIcon(iconoJapon);
        banderaP10.setIcon(iconoPolonia);
        banderaP11.setIcon(iconoSenegal);
        banderaP12.setIcon(iconoColombia);    
            
        }
        else if(contador == 49){
            
        banderaP1.setIcon(iconoRusia);
        banderaP2.setIcon(iconoArabia);
        banderaP3.setIcon(iconoEgipto);
        banderaP4.setIcon(iconoUruguay);
        banderaP5.setIcon(iconoRusia);
        banderaP6.setIcon(iconoEgipto);
        banderaP7.setIcon(iconoUruguay);
        banderaP8.setIcon(iconoArabia);
        banderaP9.setIcon(iconoUruguay);
        banderaP10.setIcon(iconoRusia);
        banderaP11.setIcon(iconoArabia);
        banderaP12.setIcon(iconoEgipto);  
            
            
        }
        
       if(contador > 0){
           
         String equipo11 =  sistema.getListaPartidos()[contador] .getEquipo1().getNombre();
         String equipo12 =  sistema.getListaPartidos()[contador] .getEquipo2().getNombre();
         String estadio1 = sistema.getListaPartidos()[contador] .getEstadio();
         String fecha1 = sistema.getListaPartidos()[contador] .getInfo();
         nombreP1.setText(equipo11);
         nombreP2.setText(equipo12);
         estadioP1.setText(fecha1 + " - " + estadio1);
         
         String equipo21 =  sistema.getListaPartidos()[contador + 1] .getEquipo1().getNombre();
         String equipo22 =  sistema.getListaPartidos()[contador + 1] .getEquipo2().getNombre();
         String estadio2 = sistema.getListaPartidos()[contador + 1] .getEstadio();
         String fecha2 = sistema.getListaPartidos()[contador + 1] .getInfo();
         nombreP3.setText(equipo21);
         nombreP4.setText(equipo22);
         estadioP2.setText(estadio2 + " - " + fecha2);
         
         String equipo31 =  sistema.getListaPartidos()[contador + 2] .getEquipo1().getNombre();
         String equipo32 =  sistema.getListaPartidos()[contador + 2] .getEquipo2().getNombre();
         String estadio3 = sistema.getListaPartidos()[contador + 2] .getEstadio();
         String fecha3 = sistema.getListaPartidos()[contador + 2] .getInfo();
         nombreP5.setText(equipo31);
         nombreP6.setText(equipo32);
         estadioP3.setText(fecha3 + " - " + estadio3);
         
         String equipo41 =  sistema.getListaPartidos()[contador + 3] .getEquipo1().getNombre();
         String equipo42 =  sistema.getListaPartidos()[contador + 3] .getEquipo2().getNombre();
         String estadio4 = sistema.getListaPartidos()[contador + 3] .getEstadio();
         String fecha4 = sistema.getListaPartidos()[contador + 3] .getInfo();
         nombreP7.setText(equipo41);
         nombreP8.setText(equipo42);
         estadioP4.setText(fecha4 + " - " + estadio4);
         
         String equipo51 =  sistema.getListaPartidos()[contador + 4] .getEquipo1().getNombre();
         String equipo52 =  sistema.getListaPartidos()[contador + 4] .getEquipo2().getNombre();
         String estadio5 = sistema.getListaPartidos()[contador + 4] .getEstadio();
         String fecha5 = sistema.getListaPartidos()[contador + 4] .getInfo();
         nombreP9.setText(equipo51);
         nombreP10.setText(equipo52);
         estadioP5.setText(fecha5 + " - " + estadio5);
         
         String equipo61 =  sistema.getListaPartidos()[contador + 5] .getEquipo1().getNombre();
         String equipo62 =  sistema.getListaPartidos()[contador + 5] .getEquipo2().getNombre();
         String estadio6 = sistema.getListaPartidos()[contador + 5] .getEstadio();
         String fecha6 = sistema.getListaPartidos()[contador + 5] .getInfo();
         nombreP11.setText(equipo61);
         nombreP12.setText(equipo62);
         estadioP6.setText(fecha6 + " - " + estadio6);
           
       }
       else{
           
           jBotonComienzo.setEnabled(false);
           contador = contador + 6;
       }
         
        
        
        
        
    }//GEN-LAST:event_jBotonComienzoActionPerformed

    private void jBotonSig1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonSig1ActionPerformed

        
        
        //if(sistema.pronosticoCompleto(jugador,1, 49)){
        frmJugarOctavos admin = new frmJugarOctavos(sistema, jugador);
        admin.setVisible(true);
        this.dispose();
            
      //  }
       // else{
            
         //   JOptionPane.showMessageDialog(this, "Para avanzar a la próxima fase debe completar totalmente la 1era Rueda"); 
            
      //  }
        
        
        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonSig1ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        frmMenuPrincipal menu = new frmMenuPrincipal(sistema);
        menu.setResizable(false);
        menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmJugar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmJugar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmJugar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmJugar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmJugar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel banderaP1;
    private javax.swing.JLabel banderaP10;
    private javax.swing.JLabel banderaP11;
    private javax.swing.JLabel banderaP12;
    private javax.swing.JLabel banderaP2;
    private javax.swing.JLabel banderaP3;
    private javax.swing.JLabel banderaP4;
    private javax.swing.JLabel banderaP5;
    private javax.swing.JLabel banderaP6;
    private javax.swing.JLabel banderaP7;
    private javax.swing.JLabel banderaP8;
    private javax.swing.JLabel banderaP9;
    private javax.swing.JLabel estadioP1;
    private javax.swing.JLabel estadioP2;
    private javax.swing.JLabel estadioP3;
    private javax.swing.JLabel estadioP4;
    private javax.swing.JLabel estadioP5;
    private javax.swing.JLabel estadioP6;
    private javax.swing.JButton jBotonComienzo;
    private javax.swing.JButton jBotonSig1;
    private javax.swing.JButton jBotonSiguiente;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabelJ;
    private javax.swing.JLabel jLabelJugador;
    private javax.swing.JLabel jLabelPuntos;
    private javax.swing.JList jListaRanking;
    private javax.swing.JTextField jP1E1Goles;
    private javax.swing.JTextField jP1E2Goles;
    private javax.swing.JTextField jP2E1Goles;
    private javax.swing.JTextField jP2E2Goles;
    private javax.swing.JTextField jP3E1Goles;
    private javax.swing.JTextField jP3E2Goles;
    private javax.swing.JTextField jP4E1Goles;
    private javax.swing.JTextField jP4E2Goles;
    private javax.swing.JTextField jP5E1Goles;
    private javax.swing.JTextField jP5E2Goles;
    private javax.swing.JTextField jP6E1Goles;
    private javax.swing.JTextField jP6E2Goles;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelFase;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel nombreP1;
    private javax.swing.JLabel nombreP10;
    private javax.swing.JLabel nombreP11;
    private javax.swing.JLabel nombreP12;
    private javax.swing.JLabel nombreP2;
    private javax.swing.JLabel nombreP3;
    private javax.swing.JLabel nombreP4;
    private javax.swing.JLabel nombreP5;
    private javax.swing.JLabel nombreP6;
    private javax.swing.JLabel nombreP7;
    private javax.swing.JLabel nombreP8;
    private javax.swing.JLabel nombreP9;
    // End of variables declaration//GEN-END:variables
}
