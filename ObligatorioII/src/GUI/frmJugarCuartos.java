/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import LOGICA.Equipo;
import LOGICA.Jugador;
import LOGICA.Pronostico;
import LOGICA.Sistema;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Diego
 */
public class frmJugarCuartos extends javax.swing.JFrame {
        Sistema sistema;
       Jugador jugador;
       int contador=57;
    /**
     * Creates new form frmJugarCuartos
     */
    public frmJugarCuartos(Sistema sis,Jugador unJ) {
        
        sistema = sis;
        jugador = unJ;
        estadioP1 = new JLabel();
        estadioP2 = new JLabel();
        estadioP3 = new JLabel();
        estadioP4 = new JLabel();
        Equipo nombreG1 = new Equipo();
        nombreG1 =sistema.devuelveGanador(contador - 8);
        Equipo nombreG2 = new Equipo();
        nombreG2 =sistema.devuelveGanador(contador - 7);
        Equipo nombreG3 = new Equipo();
        nombreG3 =sistema.devuelveGanador(contador - 6);
        Equipo nombreG4 = new Equipo();
        nombreG4 =sistema.devuelveGanador(contador - 5);
        Equipo nombreG5 = new Equipo();
        nombreG5 =sistema.devuelveGanador(contador - 4);
        Equipo nombreG6 = new Equipo();
        nombreG6 =sistema.devuelveGanador(contador - 3);
        Equipo nombreG7 = new Equipo();
        nombreG7 =sistema.devuelveGanador(contador - 2);
        Equipo nombreG8 = new Equipo();
        nombreG8 =sistema.devuelveGanador(contador - 1);
        
        
        
        setIconImage(new ImageIcon(getClass().getResource("/IMAGENES/copa.jpg")).getImage());
      
       
       ((JPanel)getContentPane()).setOpaque(false);
        ImageIcon uno=new ImageIcon(this.getClass().getResource("/IMAGENES/fondo.jpg"));
        JLabel fondo= new JLabel();
        fondo.setIcon(uno);
        
        fondo.setBounds(0,0,uno.getIconWidth(),uno.getIconHeight());
        
        getLayeredPane().add(fondo,JLayeredPane.FRAME_CONTENT_LAYER);
        
        initComponents();
        
        String estadio1 = sistema.getListaPartidos()[contador] .getEstadio();
         String fecha1 = sistema.getListaPartidos()[contador] .getInfo();
         nombreP1.setText(nombreG1.getNombre());
         nombreP2.setText(nombreG2.getNombre());
         ImageIcon iconob1= new ImageIcon(this.getClass().getResource("/IMAGENES/" + nombreG1.getNombre().toLowerCase() + ".png"));
         jBotonE1.setIcon(iconob1);
         ImageIcon iconob2 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + nombreG2.getNombre().toLowerCase() + ".png"));
         jBotonE2.setIcon(iconob2);
         estadioP1.setText(estadio1 + " - " + fecha1);
         String estadio2 = sistema.getListaPartidos()[contador + 1] .getEstadio();
         String fecha2 = sistema.getListaPartidos()[contador + 1] .getInfo();
         
         
         nombreP3.setText(nombreG3.getNombre());
         nombreP4.setText(nombreG4.getNombre());
         ImageIcon iconob3= new ImageIcon(this.getClass().getResource("/IMAGENES/" + nombreG3.getNombre().toLowerCase() + ".png"));
         jBotonE3.setIcon(iconob3);
         ImageIcon iconob4 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + nombreG4.getNombre().toLowerCase() + ".png"));
         jBotonE4.setIcon(iconob4);
         
         estadioP2.setText(estadio2 + " - " + fecha2);
         String estadio3 = sistema.getListaPartidos()[contador + 2] .getEstadio();
         String fecha3 = sistema.getListaPartidos()[contador + 2] .getInfo();
         
         
         nombreP5.setText(nombreG5.getNombre());
         nombreP6.setText(nombreG6.getNombre());
         
         ImageIcon iconob5= new ImageIcon(this.getClass().getResource("/IMAGENES/" + nombreG5.getNombre().toLowerCase() + ".png"));
         jBotonE5.setIcon(iconob5);
         ImageIcon iconob6 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + nombreG6.getNombre().toLowerCase() + ".png"));
         jBotonE6.setIcon(iconob6);
         
         estadioP3.setText(estadio3 + " - " + fecha3);
         String estadio4 = sistema.getListaPartidos()[contador + 3] .getEstadio();
         String fecha4 = sistema.getListaPartidos()[contador + 3] .getInfo();
         
         nombreP7.setText(nombreG7.getNombre());
         nombreP8.setText(nombreG8.getNombre());
         
         ImageIcon iconob7= new ImageIcon(this.getClass().getResource("/IMAGENES/" + nombreG7.getNombre().toLowerCase() + ".png"));
         jBotonE7.setIcon(iconob7);
         ImageIcon iconob8 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + nombreG8.getNombre().toLowerCase() + ".png"));
         jBotonE8.setIcon(iconob8);
         estadioP4.setText(estadio4 + " - " + fecha4);
         
         
        sistema.getListaPartidos()[contador].setEquipo1(nombreG1);
        sistema.getListaPartidos()[contador].setEquipo2(nombreG2);
        
        sistema.getListaPartidos()[contador + 1].setEquipo1(nombreG3);
        sistema.getListaPartidos()[contador + 1].setEquipo2(nombreG4);
        
        sistema.getListaPartidos()[contador + 2].setEquipo1(nombreG5);
        sistema.getListaPartidos()[contador + 2].setEquipo2(nombreG6); 
        
        sistema.getListaPartidos()[contador + 3].setEquipo1(nombreG7);
        sistema.getListaPartidos()[contador + 3].setEquipo2(nombreG8);
        
        
        
        jLabelCuartos.setText(unJ.getAlias());
        
         String aliasLista="";
        DefaultListModel listaModelo = new DefaultListModel();
        for (int i= 0 ;i < sistema.getListaJugadores().size(); i++){
            aliasLista = sistema.getListaJugadores().get(i).toString();
            listaModelo.addElement(aliasLista);
           }
         
         
         
        jListaRanking.setModel(listaModelo);
        
        
        
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private frmJugarCuartos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        nombreP2 = new javax.swing.JLabel();
        nombreP1 = new javax.swing.JLabel();
        estadioP1 = new javax.swing.JLabel();
        jBotonE2 = new javax.swing.JButton();
        jBotonE1 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        nombreP8 = new javax.swing.JLabel();
        nombreP7 = new javax.swing.JLabel();
        estadioP4 = new javax.swing.JLabel();
        jBotonE7 = new javax.swing.JButton();
        jBotonE8 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        nombreP4 = new javax.swing.JLabel();
        nombreP3 = new javax.swing.JLabel();
        estadioP2 = new javax.swing.JLabel();
        jBotonE3 = new javax.swing.JButton();
        jBotonE4 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        nombreP6 = new javax.swing.JLabel();
        nombreP5 = new javax.swing.JLabel();
        estadioP3 = new javax.swing.JLabel();
        jBotonE5 = new javax.swing.JButton();
        jBotonE6 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabelCuartos = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListaRanking = new javax.swing.JList<>();
        jLabelJugador = new javax.swing.JLabel();
        jLabelPuntos = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jBotonSig = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("JUEGO PENCA - CUARTOS DE FINAL");
        setPreferredSize(new java.awt.Dimension(500, 720));

        jPanel1.setBackground(new java.awt.Color(180, 36, 36));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP1.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE2ActionPerformed(evt);
            }
        });

        jBotonE1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreP1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(jBotonE1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBotonE2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55)
                .addComponent(nombreP2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP1, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(nombreP2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBotonE2, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(jBotonE1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP1, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP4.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE7ActionPerformed(evt);
            }
        });

        jBotonE8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreP7, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(jBotonE7, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBotonE8, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(nombreP8, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP4, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(nombreP8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(nombreP7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jBotonE7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonE8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP2.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE3ActionPerformed(evt);
            }
        });

        jBotonE4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreP3, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(jBotonE3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBotonE4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56)
                .addComponent(nombreP4, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP2, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(nombreP4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(nombreP3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jBotonE3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonE4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP2, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP3.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE5ActionPerformed(evt);
            }
        });

        jBotonE6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreP5, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(jBotonE5, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBotonE6, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(nombreP6, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP3, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(nombreP6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(nombreP5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jBotonE5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonE6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP3, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(180, 36, 36));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("JUGADOR"));

        jLabelCuartos.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jLabelCuartos, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(65, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabelCuartos, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jListaRanking.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jListaRanking);

        jLabelJugador.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabelJugador.setText("JUGADOR");

        jLabelPuntos.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabelPuntos.setText("PUNTOS");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelJugador)
                        .addGap(39, 39, 39)
                        .addComponent(jLabelPuntos)))
                .addGap(18, 18, 18))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(133, 133, 133)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelJugador, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelPuntos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel4.setPreferredSize(new java.awt.Dimension(160, 80));

        jBotonSig.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGENES/sig.png"))); // NOI18N
        jBotonSig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonSigActionPerformed(evt);
            }
        });

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGENES/salir.png"))); // NOI18N
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jBotonSig, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonSig))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(55, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(53, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 566, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:

        this.dispose();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jBotonSigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonSigActionPerformed

        
        frmJugarSemiFinal admin = new frmJugarSemiFinal(sistema, jugador);
        admin.setVisible(true);
        this.dispose();


        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonSigActionPerformed

    private void jBotonE1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE1ActionPerformed

        
        
        if(sistema.fechaValida(contador)){
         Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        unE.setNombre(nombreP1.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonE1ActionPerformed

    private void jBotonE2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE2ActionPerformed
         if(sistema.fechaValida(contador)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP2.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        
        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonE2ActionPerformed

    private void jBotonE3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE3ActionPerformed

         if(sistema.fechaValida(contador + 1)){
         Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        unE.setNombre(nombreP3.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador + 1);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonE3ActionPerformed

    private void jBotonE4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE4ActionPerformed

        if(sistema.fechaValida(contador + 2)){
          Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP4.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador + 2);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonE4ActionPerformed

    private void jBotonE5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE5ActionPerformed

        if(sistema.fechaValida(contador + 2)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        unE.setNombre(nombreP5.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador + 2);
        sistema.actualizaRanking(contador);
        JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
       
        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonE5ActionPerformed

    private void jBotonE6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE6ActionPerformed

        if(sistema.fechaValida(contador + 2)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP6.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador + 2);
        sistema.actualizaRanking(contador);
        JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        
    }//GEN-LAST:event_jBotonE6ActionPerformed

    private void jBotonE7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE7ActionPerformed
        
        if(sistema.fechaValida(contador + 3)){
         Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP7.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador + 3);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        
    }//GEN-LAST:event_jBotonE7ActionPerformed

    private void jBotonE8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE8ActionPerformed
        if(sistema.fechaValida(contador + 3)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP8.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador + 3);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }

        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonE8ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmJugarCuartos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmJugarCuartos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmJugarCuartos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmJugarCuartos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmJugarCuartos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel estadioP1;
    private javax.swing.JLabel estadioP2;
    private javax.swing.JLabel estadioP3;
    private javax.swing.JLabel estadioP4;
    private javax.swing.JButton jBotonE1;
    private javax.swing.JButton jBotonE2;
    private javax.swing.JButton jBotonE3;
    private javax.swing.JButton jBotonE4;
    private javax.swing.JButton jBotonE5;
    private javax.swing.JButton jBotonE6;
    private javax.swing.JButton jBotonE7;
    private javax.swing.JButton jBotonE8;
    private javax.swing.JButton jBotonSig;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabelCuartos;
    private javax.swing.JLabel jLabelJugador;
    private javax.swing.JLabel jLabelPuntos;
    private javax.swing.JList<String> jListaRanking;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel nombreP1;
    private javax.swing.JLabel nombreP2;
    private javax.swing.JLabel nombreP3;
    private javax.swing.JLabel nombreP4;
    private javax.swing.JLabel nombreP5;
    private javax.swing.JLabel nombreP6;
    private javax.swing.JLabel nombreP7;
    private javax.swing.JLabel nombreP8;
    // End of variables declaration//GEN-END:variables
}
