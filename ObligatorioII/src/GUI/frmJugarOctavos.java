/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import LOGICA.Equipo;
import LOGICA.Jugador;
import LOGICA.Partido;
import LOGICA.Pronostico;
import LOGICA.Sistema;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Diego
 */
public class frmJugarOctavos extends javax.swing.JFrame {
       Sistema sistema;
       Jugador jugador;
       int contador=49;
    /**
     * Creates new form frmJugarOctavos
     */
    public frmJugarOctavos(Sistema sis,Jugador unJ) {
        sistema = sis;
        jugador = unJ;
        estadioP1 = new JLabel();
        estadioP2 = new JLabel();
        estadioP3 = new JLabel();
        estadioP4 = new JLabel();
        estadioP5 = new JLabel();
        estadioP6 = new JLabel();
        estadioP7 = new JLabel();
        estadioP8 = new JLabel();
        
        
        
        
        
        setIconImage(new ImageIcon(getClass().getResource("/IMAGENES/copa.jpg")).getImage());
       
       
       ((JPanel)getContentPane()).setOpaque(false);
        ImageIcon uno=new ImageIcon(this.getClass().getResource("/IMAGENES/fondo.jpg"));
        JLabel fondo= new JLabel();
        fondo.setIcon(uno);
        
        fondo.setBounds(0,0,uno.getIconWidth(),uno.getIconHeight());
        
        
        
        Equipo primeroA = new Equipo();
        primeroA = sistema.devuelveEquipo(1, 4);
        Equipo segundoA = new Equipo();
        segundoA = sistema.devuelveEquipo(1, 4);
        Equipo primeroB = new Equipo();
        primeroB = sistema.devuelveEquipo(5, 8);
        Equipo segundoB = new Equipo();
        segundoB = sistema.devuelveEquipo(5, 8);
        Equipo primeroC = new Equipo();
        primeroC = sistema.devuelveEquipo(9, 12);
        Equipo segundoC = new Equipo();
        segundoC = sistema.devuelveEquipo(9, 12);
        Equipo primeroD = new Equipo();
        primeroD = sistema.devuelveEquipo(13, 16);
        Equipo segundoD = new Equipo();
        segundoD = sistema.devuelveEquipo(13, 16);
        Equipo primeroE = new Equipo();
        primeroE = sistema.devuelveEquipo(17, 20);
        Equipo segundoE = new Equipo();
        segundoE = sistema.devuelveEquipo(17, 20);
        Equipo primeroF = new Equipo();
        primeroF = sistema.devuelveEquipo(21, 24);
        Equipo segundoF = new Equipo();
        segundoF = sistema.devuelveEquipo(21, 24);
        Equipo primeroG = new Equipo();
        primeroG = sistema.devuelveEquipo(25, 28);
        Equipo segundoG = new Equipo();
        segundoG = sistema.devuelveEquipo(25, 28);
        Equipo primeroH = new Equipo();
        primeroH = sistema.devuelveEquipo(29, 32);
        Equipo segundoH = new Equipo();
        segundoH = sistema.devuelveEquipo(29, 32);
        
        getLayeredPane().add(fondo,JLayeredPane.FRAME_CONTENT_LAYER);
         initComponents();
         
         
         String estadio1 = sistema.getListaPartidos()[contador] .getEstadio();
         String fecha1 = sistema.getListaPartidos()[contador] .getInfo();
         nombreP1.setText(primeroA.getNombre());
         nombreP2.setText(segundoB.getNombre());
         ImageIcon iconob1= new ImageIcon(this.getClass().getResource("/IMAGENES/" + primeroA.getNombre().toLowerCase() + ".png"));
         jBotonE1.setIcon(iconob1);
         ImageIcon iconob2 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + segundoB.getNombre().toLowerCase() + ".png"));
         jBotonE2.setIcon(iconob2);                                            
         estadioP1.setText(estadio1 + " - " + fecha1);
         
         String estadio2 = sistema.getListaPartidos()[contador + 1] .getEstadio();
         String fecha2 = sistema.getListaPartidos()[contador + 1] .getInfo();
         nombreP3.setText(primeroC.getNombre());
         nombreP4.setText(segundoD.getNombre());
         ImageIcon iconob3= new ImageIcon(this.getClass().getResource("/IMAGENES/" + primeroC.getNombre().toLowerCase() + ".png"));
         jBotonE3.setIcon(iconob3);
         ImageIcon iconob4 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + segundoD.getNombre().toLowerCase() + ".png"));
         jBotonE4.setIcon(iconob4);
         estadioP2                                                                                                                                                                                         .setText(estadio2 + " - " + fecha2);
         
         String estadio3 = sistema.getListaPartidos()[contador + 2] .getEstadio();
         String fecha3 = sistema.getListaPartidos()[contador + 2] .getInfo();
         nombreP5.setText(primeroB.getNombre());
         nombreP6.setText(segundoA.getNombre());
         ImageIcon iconob5= new ImageIcon(this.getClass().getResource("/IMAGENES/" + primeroB.getNombre().toLowerCase() + ".png"));
         jBotonE5.setIcon(iconob5);
         ImageIcon iconob6 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + segundoA.getNombre().toLowerCase() + ".png"));
         jBotonE6.setIcon(iconob6);
         estadioP3.setText(estadio3 + " - " + fecha3);
         
         String estadio4 = sistema.getListaPartidos()[contador + 3] .getEstadio();
         String fecha4 = sistema.getListaPartidos()[contador + 3] .getInfo();
         nombreP7.setText(primeroD.getNombre());
         nombreP8.setText(segundoC.getNombre());
         ImageIcon iconob7= new ImageIcon(this.getClass().getResource("/IMAGENES/" + primeroD.getNombre().toLowerCase() + ".png"));
         jBotonE7.setIcon(iconob7);
         ImageIcon iconob8 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + segundoC.getNombre().toLowerCase() + ".png"));
         jBotonE8.setIcon(iconob8);
         estadioP4.setText(estadio4 + " - " + fecha4);
         
         String estadio5 = sistema.getListaPartidos()[contador + 4] .getEstadio();
         String fecha5 = sistema.getListaPartidos()[contador + 4] .getInfo();
         nombreP9.setText(primeroE.getNombre());
         nombreP10.setText(segundoF.getNombre());
         ImageIcon iconob9= new ImageIcon(this.getClass().getResource("/IMAGENES/" + primeroE.getNombre().toLowerCase() + ".png"));
         jBotonE9.setIcon(iconob9);
         ImageIcon iconob10 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + segundoF.getNombre().toLowerCase() + ".png"));
         jBotonE10.setIcon(iconob10);
         estadioP5.setText(estadio5 + " - " + fecha5);
         
         String estadio6 = sistema.getListaPartidos()[contador + 5] .getEstadio();
         String fecha6 = sistema.getListaPartidos()[contador + 5] .getInfo();
         nombreP11.setText(primeroG.getNombre());
         nombreP12.setText(segundoH.getNombre());
         ImageIcon iconob11= new ImageIcon(this.getClass().getResource("/IMAGENES/" + primeroG.getNombre().toLowerCase() + ".png"));
         jBotonE11.setIcon(iconob11);
         ImageIcon iconob12 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + segundoH.getNombre().toLowerCase() + ".png"));
         jBotonE12.setIcon(iconob12);
         estadioP6.setText(estadio6 + " - " + fecha6);
         
         String estadio7 = sistema.getListaPartidos()[contador + 6] .getEstadio();
         String fecha7 = sistema.getListaPartidos()[contador + 6] .getInfo();
         nombreP13.setText(primeroF.getNombre());
         nombreP14.setText(segundoE.getNombre());
         ImageIcon iconob13= new ImageIcon(this.getClass().getResource("/IMAGENES/" + primeroF.getNombre().toLowerCase() + ".png"));
         jBotonE13.setIcon(iconob13);
         ImageIcon iconob14 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + segundoE.getNombre().toLowerCase() + ".png"));
         jBotonE14.setIcon(iconob14);
         estadioP7.setText(estadio7 + " - " + fecha7);
         
         String estadio8 = sistema.getListaPartidos()[contador + 7] .getEstadio();
         String fecha8 = sistema.getListaPartidos()[contador + 7] .getInfo();
         nombreP15.setText(primeroH.getNombre());
         nombreP16.setText(segundoG.getNombre());
         ImageIcon iconob15= new ImageIcon(this.getClass().getResource("/IMAGENES/" + primeroH.getNombre().toLowerCase() + ".png"));
         jBotonE15.setIcon(iconob15);
         ImageIcon iconob16 = new ImageIcon(this.getClass().getResource("/IMAGENES/" + segundoG.getNombre().toLowerCase() + ".png"));
         jBotonE16.setIcon(iconob16);
         estadioP8.setText(estadio8 + " - " + fecha8);
         
        
        sistema.getListaPartidos()[contador].getEquipo1().setNombre(primeroA.getNombre());
        sistema.getListaPartidos()[contador].getEquipo2().setNombre(segundoB.getNombre());
        
        sistema.getListaPartidos()[contador + 1].getEquipo1().setNombre(primeroC.getNombre());
        sistema.getListaPartidos()[contador + 1].getEquipo2().setNombre(segundoD.getNombre());
        
        sistema.getListaPartidos()[contador + 2].getEquipo1().setNombre(primeroB.getNombre());
        sistema.getListaPartidos()[contador + 2].getEquipo2().setNombre(segundoA.getNombre()); 
        
        sistema.getListaPartidos()[contador + 3].getEquipo1().setNombre(primeroD.getNombre());
        sistema.getListaPartidos()[contador + 3].getEquipo2().setNombre(segundoC.getNombre());
        
        sistema.getListaPartidos()[contador + 4].getEquipo1().setNombre(primeroE.getNombre());
        sistema.getListaPartidos()[contador + 4].getEquipo2().setNombre(segundoF.getNombre());
        
        sistema.getListaPartidos()[contador + 5].getEquipo1().setNombre(primeroG.getNombre());
        sistema.getListaPartidos()[contador + 5].getEquipo2().setNombre(segundoH.getNombre());
        
        sistema.getListaPartidos()[contador + 6].getEquipo1().setNombre(primeroF.getNombre());
        sistema.getListaPartidos()[contador + 6].getEquipo2().setNombre(segundoE.getNombre());
        
        sistema.getListaPartidos()[contador + 7].getEquipo1().setNombre(primeroH.getNombre());
        sistema.getListaPartidos()[contador + 7].getEquipo2().setNombre(segundoG.getNombre());
        
        
         
            initComponents();
        
        setLocationRelativeTo(null);
        setVisible(true);
        
        
       
        

        
    }

    private frmJugarOctavos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        nombreP16 = new javax.swing.JLabel();
        nombreP15 = new javax.swing.JLabel();
        estadioP8 = new javax.swing.JLabel();
        jBotonE15 = new javax.swing.JButton();
        jBotonE16 = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        nombreP14 = new javax.swing.JLabel();
        nombreP13 = new javax.swing.JLabel();
        estadioP7 = new javax.swing.JLabel();
        jBotonE13 = new javax.swing.JButton();
        jBotonE14 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        nombreP2 = new javax.swing.JLabel();
        nombreP1 = new javax.swing.JLabel();
        estadioP1 = new javax.swing.JLabel();
        jBotonE1 = new javax.swing.JButton();
        jBotonE2 = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        nombreP12 = new javax.swing.JLabel();
        nombreP11 = new javax.swing.JLabel();
        estadioP6 = new javax.swing.JLabel();
        jBotonE11 = new javax.swing.JButton();
        jBotonE12 = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        nombreP10 = new javax.swing.JLabel();
        nombreP9 = new javax.swing.JLabel();
        estadioP5 = new javax.swing.JLabel();
        jBotonE9 = new javax.swing.JButton();
        jBotonE10 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        nombreP8 = new javax.swing.JLabel();
        nombreP7 = new javax.swing.JLabel();
        estadioP4 = new javax.swing.JLabel();
        jBotonE7 = new javax.swing.JButton();
        jBotonE8 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        nombreP4 = new javax.swing.JLabel();
        nombreP3 = new javax.swing.JLabel();
        estadioP2 = new javax.swing.JLabel();
        jBotonE4 = new javax.swing.JButton();
        jBotonE3 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        nombreP6 = new javax.swing.JLabel();
        nombreP5 = new javax.swing.JLabel();
        estadioP3 = new javax.swing.JLabel();
        jBotonE5 = new javax.swing.JButton();
        jBotonE6 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jBotonSig = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("JUEGO PENCA - OCTAVOS DE FINAL");
        setPreferredSize(new java.awt.Dimension(500, 720));

        jPanel1.setBackground(new java.awt.Color(180, 36, 36));

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP8.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE15.setMaximumSize(new java.awt.Dimension(33, 39));
        jBotonE15.setMinimumSize(new java.awt.Dimension(33, 39));
        jBotonE15.setPreferredSize(new java.awt.Dimension(33, 39));
        jBotonE15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE15ActionPerformed(evt);
            }
        });

        jBotonE16.setMaximumSize(new java.awt.Dimension(33, 39));
        jBotonE16.setMinimumSize(new java.awt.Dimension(33, 39));
        jBotonE16.setPreferredSize(new java.awt.Dimension(33, 39));
        jBotonE16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE16ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreP15, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addComponent(jBotonE15, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBotonE16, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addComponent(nombreP16, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP8, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nombreP16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBotonE15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonE16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP8, javax.swing.GroupLayout.DEFAULT_SIZE, 15, Short.MAX_VALUE))
        );

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP7.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE13.setMaximumSize(new java.awt.Dimension(33, 39));
        jBotonE13.setMinimumSize(new java.awt.Dimension(33, 39));
        jBotonE13.setPreferredSize(new java.awt.Dimension(33, 39));
        jBotonE13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE13ActionPerformed(evt);
            }
        });

        jBotonE14.setMaximumSize(new java.awt.Dimension(33, 39));
        jBotonE14.setMinimumSize(new java.awt.Dimension(33, 39));
        jBotonE14.setPreferredSize(new java.awt.Dimension(33, 39));
        jBotonE14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE14ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreP13, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(jBotonE13, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBotonE14, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(53, 53, 53)
                .addComponent(nombreP14, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP7, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nombreP14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBotonE13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonE14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(estadioP7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(4, 4, 4))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP1.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE1.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jBotonE1.setMaximumSize(new java.awt.Dimension(39, 39));
        jBotonE1.setMinimumSize(new java.awt.Dimension(39, 39));
        jBotonE1.setPreferredSize(new java.awt.Dimension(39, 39));
        jBotonE1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE1ActionPerformed(evt);
            }
        });

        jBotonE2.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jBotonE2.setMaximumSize(new java.awt.Dimension(39, 39));
        jBotonE2.setMinimumSize(new java.awt.Dimension(39, 39));
        jBotonE2.setPreferredSize(new java.awt.Dimension(39, 39));
        jBotonE2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(nombreP1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42)
                        .addComponent(jBotonE1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jBotonE2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(56, 56, 56)
                        .addComponent(nombreP2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addComponent(estadioP1, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nombreP2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBotonE2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonE1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP1, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP6.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE11.setMaximumSize(new java.awt.Dimension(33, 39));
        jBotonE11.setMinimumSize(new java.awt.Dimension(33, 39));
        jBotonE11.setPreferredSize(new java.awt.Dimension(33, 39));
        jBotonE11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE11ActionPerformed(evt);
            }
        });

        jBotonE12.setMaximumSize(new java.awt.Dimension(33, 39));
        jBotonE12.setMinimumSize(new java.awt.Dimension(33, 39));
        jBotonE12.setPreferredSize(new java.awt.Dimension(33, 39));
        jBotonE12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE12ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreP11, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(jBotonE11, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBotonE12, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addComponent(nombreP12, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP6, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nombreP12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBotonE11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonE12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(estadioP6, javax.swing.GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE))
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP5.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE9.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jBotonE9.setMaximumSize(new java.awt.Dimension(39, 39));
        jBotonE9.setMinimumSize(new java.awt.Dimension(39, 39));
        jBotonE9.setPreferredSize(new java.awt.Dimension(39, 39));
        jBotonE9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE9ActionPerformed(evt);
            }
        });

        jBotonE10.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jBotonE10.setMaximumSize(new java.awt.Dimension(39, 39));
        jBotonE10.setMinimumSize(new java.awt.Dimension(39, 39));
        jBotonE10.setPreferredSize(new java.awt.Dimension(39, 39));
        jBotonE10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreP9, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(jBotonE9, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBotonE10, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addComponent(nombreP10, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP5, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nombreP10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBotonE9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonE10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP5, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP4.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE7.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jBotonE7.setMaximumSize(new java.awt.Dimension(39, 39));
        jBotonE7.setMinimumSize(new java.awt.Dimension(39, 39));
        jBotonE7.setPreferredSize(new java.awt.Dimension(39, 39));
        jBotonE7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE7ActionPerformed(evt);
            }
        });

        jBotonE8.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jBotonE8.setMaximumSize(new java.awt.Dimension(39, 39));
        jBotonE8.setMinimumSize(new java.awt.Dimension(39, 39));
        jBotonE8.setPreferredSize(new java.awt.Dimension(39, 39));
        jBotonE8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreP7, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(jBotonE7, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBotonE8, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(nombreP8, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP4, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(nombreP8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(nombreP7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jBotonE7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonE8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estadioP4, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP2.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE4.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jBotonE4.setMaximumSize(new java.awt.Dimension(39, 39));
        jBotonE4.setMinimumSize(new java.awt.Dimension(39, 39));
        jBotonE4.setPreferredSize(new java.awt.Dimension(39, 39));
        jBotonE4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE4ActionPerformed(evt);
            }
        });

        jBotonE3.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jBotonE3.setMaximumSize(new java.awt.Dimension(39, 39));
        jBotonE3.setMinimumSize(new java.awt.Dimension(39, 39));
        jBotonE3.setPreferredSize(new java.awt.Dimension(39, 39));
        jBotonE3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreP3, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jBotonE3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(jBotonE4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48)
                        .addComponent(nombreP4, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(estadioP2, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nombreP4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreP3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBotonE4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonE3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP2, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        nombreP6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        nombreP5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        estadioP3.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N

        jBotonE5.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jBotonE5.setMaximumSize(new java.awt.Dimension(39, 39));
        jBotonE5.setMinimumSize(new java.awt.Dimension(39, 39));
        jBotonE5.setPreferredSize(new java.awt.Dimension(39, 39));
        jBotonE5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE5ActionPerformed(evt);
            }
        });

        jBotonE6.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jBotonE6.setMaximumSize(new java.awt.Dimension(39, 39));
        jBotonE6.setMinimumSize(new java.awt.Dimension(39, 39));
        jBotonE6.setPreferredSize(new java.awt.Dimension(39, 39));
        jBotonE6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonE6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nombreP5, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(jBotonE5, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jBotonE6, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(nombreP6, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(estadioP3, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(nombreP6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(nombreP5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jBotonE5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonE6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(estadioP3, javax.swing.GroupLayout.DEFAULT_SIZE, 16, Short.MAX_VALUE)
                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );

        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel2.setPreferredSize(new java.awt.Dimension(160, 80));

        jBotonSig.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGENES/sig.png"))); // NOI18N
        jBotonSig.setMaximumSize(new java.awt.Dimension(50, 50));
        jBotonSig.setMinimumSize(new java.awt.Dimension(50, 50));
        jBotonSig.setPreferredSize(new java.awt.Dimension(50, 50));
        jBotonSig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonSigActionPerformed(evt);
            }
        });

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGENES/salir.png"))); // NOI18N
        jButton5.setPreferredSize(new java.awt.Dimension(80, 60));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jBotonSig, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jButton5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jBotonSig, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(171, 171, 171)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        frmMenuPrincipal menu = new frmMenuPrincipal(sistema);
        menu.setResizable(false);
        menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jBotonSigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonSigActionPerformed

         sistema.actualizaRanking(contador);
        frmJugarCuartos admin = new frmJugarCuartos(sistema, jugador);
        admin.setVisible(true);
        this.dispose();
       
        //this.dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonSigActionPerformed

    private void jBotonE6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE6ActionPerformed
        if(sistema.fechaValida(contador + 2)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP6.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador + 2);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
    }//GEN-LAST:event_jBotonE6ActionPerformed

    private void jBotonE5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE5ActionPerformed

        // TODO add your handling code here:
        if(sistema.fechaValida(contador + 2)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        unE.setNombre(nombreP5.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador + 2);
        sistema.actualizaRanking(contador);
     JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
    }//GEN-LAST:event_jBotonE5ActionPerformed

    private void jBotonE3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE3ActionPerformed
    if(sistema.fechaValida(contador + 1)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        unE.setNombre(nombreP3.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador + 1);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
         }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonE3ActionPerformed

    private void jBotonE4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE4ActionPerformed
    if(sistema.fechaValida(contador + 1)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP4.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador + 1);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
         }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonE4ActionPerformed

    private void jBotonE8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE8ActionPerformed
    if(sistema.fechaValida(contador + 3)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP8.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador + 3);
        sistema.actualizaRanking(contador);
        JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
    }//GEN-LAST:event_jBotonE8ActionPerformed

    private void jBotonE7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE7ActionPerformed

        // TODO add your handling code here:
        if(sistema.fechaValida(contador + 3)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        unE.setNombre(nombreP7.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador + 3);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        
    }//GEN-LAST:event_jBotonE7ActionPerformed

    private void jBotonE10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE10ActionPerformed
        if(sistema.fechaValida(contador + 4)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP10.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador + 4);
        sistema.actualizaRanking(contador);
        JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        
    }//GEN-LAST:event_jBotonE10ActionPerformed

    private void jBotonE9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE9ActionPerformed

        // TODO add your handling code here:
        if(sistema.fechaValida(contador + 4)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        unE.setNombre(nombreP9.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador + 4);
        sistema.actualizaRanking(contador);
        JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        
    }//GEN-LAST:event_jBotonE9ActionPerformed

    private void jBotonE12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE12ActionPerformed
        if(sistema.fechaValida(contador + 5)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP12.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador + 5);
        sistema.actualizaRanking(contador);
       JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
    }//GEN-LAST:event_jBotonE12ActionPerformed

    private void jBotonE11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE11ActionPerformed
        if(sistema.fechaValida(contador + 5)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        unE.setNombre(nombreP11.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador + 5);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        
    }//GEN-LAST:event_jBotonE11ActionPerformed

    private void jBotonE2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE2ActionPerformed
    if(sistema.fechaValida(contador)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP2.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador);
        sistema.actualizaRanking(contador);
       JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
        // TODO add your handling code here:
    }//GEN-LAST:event_jBotonE2ActionPerformed

    private void jBotonE1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE1ActionPerformed
    if(sistema.fechaValida(contador)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        unE.setNombre(nombreP1.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador);
        sistema.actualizaRanking(contador);
        JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        // TODO add your handling code here:
    }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           }
    }//GEN-LAST:event_jBotonE1ActionPerformed
    
    private void jBotonE14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE14ActionPerformed
    if(sistema.fechaValida(contador + 6)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP14.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador + 6);
        sistema.actualizaRanking(contador);
       JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
       }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           } 
        
    }//GEN-LAST:event_jBotonE14ActionPerformed

    private void jBotonE13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE13ActionPerformed
        if(sistema.fechaValida(contador + 6)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        unE.setNombre(nombreP13.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador + 6);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           } 
    }//GEN-LAST:event_jBotonE13ActionPerformed

    private void jBotonE16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE16ActionPerformed
    if(sistema.fechaValida(contador + 7)){
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        dosE.setNombre(nombreP16.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(0);
        unP.setGolesEquipo2(2);
        jugador.agregarPronostico(unP, contador + 7);
        sistema.actualizaRanking(contador);
        JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
        // TODO add your handling code here:
        }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           } 
    }//GEN-LAST:event_jBotonE16ActionPerformed

    private void jBotonE15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonE15ActionPerformed

        if(sistema.fechaValida(contador + 7)){
      
        Pronostico unP = new Pronostico();
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        unE.setNombre(nombreP15.getText());
        unP.setEquipo1(unE);
        unP.setEquipo2(dosE);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        jugador.agregarPronostico(unP, contador + 7);
        sistema.actualizaRanking(contador);
         JOptionPane.showMessageDialog(this, "Se agrego pronóstico satisfactoriamente.");
         }
      else{
            JOptionPane.showMessageDialog(this, "Ya no es posible realizar el pronóstico, caducó la fecha");
              
           } 
    }//GEN-LAST:event_jBotonE15ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmJugarOctavos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmJugarOctavos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmJugarOctavos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmJugarOctavos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmJugarOctavos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel estadioP1;
    private javax.swing.JLabel estadioP2;
    private javax.swing.JLabel estadioP3;
    private javax.swing.JLabel estadioP4;
    private javax.swing.JLabel estadioP5;
    private javax.swing.JLabel estadioP6;
    private javax.swing.JLabel estadioP7;
    private javax.swing.JLabel estadioP8;
    private javax.swing.JButton jBotonE1;
    private javax.swing.JButton jBotonE10;
    private javax.swing.JButton jBotonE11;
    private javax.swing.JButton jBotonE12;
    private javax.swing.JButton jBotonE13;
    private javax.swing.JButton jBotonE14;
    private javax.swing.JButton jBotonE15;
    private javax.swing.JButton jBotonE16;
    private javax.swing.JButton jBotonE2;
    private javax.swing.JButton jBotonE3;
    private javax.swing.JButton jBotonE4;
    private javax.swing.JButton jBotonE5;
    private javax.swing.JButton jBotonE6;
    private javax.swing.JButton jBotonE7;
    private javax.swing.JButton jBotonE8;
    private javax.swing.JButton jBotonE9;
    private javax.swing.JButton jBotonSig;
    private javax.swing.JButton jButton5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JLabel nombreP1;
    private javax.swing.JLabel nombreP10;
    private javax.swing.JLabel nombreP11;
    private javax.swing.JLabel nombreP12;
    private javax.swing.JLabel nombreP13;
    private javax.swing.JLabel nombreP14;
    private javax.swing.JLabel nombreP15;
    private javax.swing.JLabel nombreP16;
    private javax.swing.JLabel nombreP2;
    private javax.swing.JLabel nombreP3;
    private javax.swing.JLabel nombreP4;
    private javax.swing.JLabel nombreP5;
    private javax.swing.JLabel nombreP6;
    private javax.swing.JLabel nombreP7;
    private javax.swing.JLabel nombreP8;
    private javax.swing.JLabel nombreP9;
    // End of variables declaration//GEN-END:variables
}
