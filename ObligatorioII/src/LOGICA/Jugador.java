/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

/**
 *
 * @author Diego
 */
public class Jugador implements Comparable<Jugador> {
    //Declaración de atributos
    
    private String alias = "";
    private int puntos = 0;
    private Pronostico [] listaPronostico = new Pronostico [65];
    //Constructor sin parámetros
    public Jugador(){
    }
    
    //Constructor por parámetros
    public Jugador(String alias,int puntos) {
        
        
        this.setAlias(alias);
        this.setPuntos(puntos);
        
}
    
    
    //Métodos get´s y set´s
   

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
    
    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos= puntos;
    }

    public Pronostico[] getListaPronostico() {
        
        return listaPronostico;
    }

    public void setListaPronostico(Pronostico[] listaPronostico) {
        this.listaPronostico = listaPronostico;
    }
    
    public int compareTo(Jugador o) {
        return o.getPuntos() -this.getPuntos();
      
    }
    
     public void agregarPronostico(Pronostico unP,int posicion){
    
    Pronostico p = new Pronostico();     
    p.setGolesEquipo1(unP.getGolesEquipo1());
    p.setGolesEquipo2(unP.getGolesEquipo2());
    this.listaPronostico[posicion]=p;
    
    }

    @Override
    public String toString() {
        return  alias + "                            " + puntos;
    }
    
}