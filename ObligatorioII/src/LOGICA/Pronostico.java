/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

/**
 *
 * @author Diego
 */
public class Pronostico {
    
    //Declaración de atributos
    private Equipo equipo1;
    private Equipo equipo2;
    private int golesEquipo1= 0;
    private int golesEquipo2 = 0;
    
    //Constructor sin parámetros
    public Pronostico(){
    }
    
    //Constructor por parámetros
    public Pronostico(int golEquipo1,int golEquipo2) {
        

        this.setGolesEquipo1(golesEquipo1);
        this.setGolesEquipo2(golesEquipo2);
         
        
        
}
    
    
    //Métodos get´s y set´s

    public Equipo getEquipo1() {
        return equipo1;
    }

    public void setEquipo1(Equipo equipo1) {
        this.equipo1 = equipo1;
    }

    public Equipo getEquipo2() {
        return equipo2;
    }

    public void setEquipo2(Equipo equipo2) {
        this.equipo2 = equipo2;
    }

    public int getGolesEquipo1() {
        return golesEquipo1;
    }

    public void setGolesEquipo1(int golesEquipo1) {
        this.golesEquipo1 = golesEquipo1;
    }

    public int getGolesEquipo2() {
        return golesEquipo2;
    }

    public void setGolesEquipo2(int golesEquipo2) {
        this.golesEquipo2 = golesEquipo2;
    }


  
    
    

    
    
    
}
