/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

/**
 *
 * @author Diego
 */
public class Partido {
    
    
   
    //Declaración de atributos
    private Equipo equipo1;
    private Equipo equipo2;
    private int golesEquipo1= 0;
    private int golesEquipo2 = 0;
    private String estadio="";
    private String info="";
    private int mes=0;
    private int dia=0;
    private int hora=0;
    private int minutos=0;
    
    
    //Constructor sin parámetros
    public Partido(){
    }
    
    //Constructor por parámetros
    public Partido(Equipo equipo1,Equipo equipo2,int golesEquipo1,int golesEquipo2,String estadio,String info,int mes, int dia, int hora, int minutos) {
        
        
        this.setEquipo1(equipo1);
        this.setEquipo2(equipo2);
        this.setGolEquipo1(golesEquipo1);
        this.setGolEquipo2(golesEquipo2);
        this.setEstadio(estadio);
        this.setInfo(info);
        this.setMes(mes);
        this.setDia(dia);
        this.setHora(hora);
        this.setMinutos(minutos);
        
        
}
    
    
    //Métodos get´s y set´s

    public Equipo getEquipo1() {
        return equipo1;
    }

    public void setEquipo1(Equipo equipo1) {
        this.equipo1 = equipo1;
    }

    public Equipo getEquipo2() {
        return equipo2;
    }

    public void setEquipo2(Equipo equipo2) {
        this.equipo2 = equipo2;
    }

    public int getGolEquipo1() {
        return golesEquipo1;
    }

    public void setGolEquipo1(int golEquipo1) {
        this.golesEquipo1 = golEquipo1;
    }

    public int getGolEquipo2() {
        return golesEquipo2;
    }

    public void setGolEquipo2(int golEquipo2) {
        this.golesEquipo2 = golEquipo2;
    }

    public int getGolesEquipo1() {
        return golesEquipo1;
    }

    public void setGolesEquipo1(int golesEquipo1) {
        this.golesEquipo1 = golesEquipo1;
    }

    public int getGolesEquipo2() {
        return golesEquipo2;
    }

    public void setGolesEquipo2(int golesEquipo2) {
        this.golesEquipo2 = golesEquipo2;
    }

    public String getEstadio() {
        return estadio;
    }

    public void setEstadio(String estadio) {
        this.estadio = estadio;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }


    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

 

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }
    
    

    
    
}
