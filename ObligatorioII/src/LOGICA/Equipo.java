/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

/**
 *
 * @author Diego
 */
public class Equipo {
    
   
    //Declaración de atributos
    private String nombre = "";
    
    private int golesAFavor=0;
    private int golesEnContra=0;
    private int goles = golesAFavor - golesEnContra;
    
    private int puntos = 0;
    //Constructor sin parámetros
    public Equipo(){
    }
    
    //Constructor por parámetros
    public Equipo(String nombre) {
        
        
        this.setNombre(nombre);
        
        
}
    
    
    //Métodos get´s y set´s
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

   
    
    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos= puntos;
    }

    public int getGoles() {
        return goles;
    }

    public void setGoles(int goles) {
        this.goles = goles;
    }

    public int getGolesAFavor() {
        return golesAFavor;
    }

    public void setGolesAFavor(int golesAFavor) {
        this.golesAFavor = golesAFavor;
    }

    public int getGolesEnContra() {
        return golesEnContra;
    }

    public void setGolesEnContra(int golesEnContra) {
        this.golesEnContra = golesEnContra;
    }
    
    
    
    
    
    
}
