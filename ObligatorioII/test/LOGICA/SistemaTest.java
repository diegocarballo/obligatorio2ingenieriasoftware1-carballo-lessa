/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.DefaultListModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Diego
 */
public class SistemaTest {
    
    public SistemaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaJugadores method, of class Sistema.
     */
    @Test
    public void testGetListaJugadores() {
        
        
        Sistema instance = new Sistema();
        ArrayList<Jugador> unaLista = new ArrayList<Jugador>();
        instance.setListaJugadores(unaLista);
        ArrayList<Jugador> expResult = unaLista;
        ArrayList<Jugador> result = instance.getListaJugadores();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setListaJugadores method, of class Sistema.
     */
    @Test
    public void testSetListaJugadores() {
        
       
        ArrayList<Jugador> listaJugadores = null;
        Sistema instance = new Sistema();
        instance.setListaJugadores(listaJugadores);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of getListaEquipos method, of class Sistema.
     */
    @Test
    public void testGetListaEquipos() {
     
        Sistema instance = new Sistema();
        Equipo[] listaEquipos = new Equipo[5];
        instance.setListaEquipos(listaEquipos);
        Equipo[] expResult = listaEquipos;
        Equipo[] result = instance.getListaEquipos();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setListaEquipos method, of class Sistema.
     */
    @Test
    public void testSetListaEquipos() {
        
        Equipo[] listaEquipos = null;
        Sistema instance = new Sistema();
        
        instance.setListaEquipos(listaEquipos);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getListaPartidos method, of class Sistema.
     */
    @Test
    public void testGetListaPartidos() {
        
        
         Partido[] listaPartidos = new Partido[5];
        
        Sistema instance = new Sistema();
        instance.setListaPartidos(listaPartidos);
        Partido[] expResult = listaPartidos;
        Partido[] result = instance.getListaPartidos();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setListaPartidos method, of class Sistema.
     */
    @Test
    public void testSetListaPartidos() {
        
        Partido[] listaPartidos = null;
        Sistema instance = new Sistema();
        instance.setListaPartidos(listaPartidos);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getListaPronostico method, of class Sistema.
     */
    @Test
    public void testGetListaPronostico() {
        
        Pronostico[] listaPronostico = new Pronostico[5];
        
        Sistema instance = new Sistema();
        instance.setListaPronostico(listaPronostico);
        
        Pronostico[] expResult = listaPronostico;
        Pronostico[] result = instance.getListaPronostico();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setListaPronostico method, of class Sistema.
     */
    @Test
    public void testSetListaPronostico() {
        
        Pronostico[] listaPronostico = null;
        Sistema instance = new Sistema();
        instance.setListaPronostico(listaPronostico);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of agregarPartido method, of class Sistema.
     */
    @Test
    public void testAgregarPartido() {
       
        
        
        
        Partido unP = new Partido();
        int posicion = 1;
        Sistema instance = new Sistema();
        instance.agregarPartido(unP, posicion);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    

    /**
     * Test of getPuntosGanador method, of class Sistema.
     */
    @Test
    public void testGetPuntosGanador() {
        int puntosGanador = 5;
        Sistema instance = new Sistema();
        instance.setPuntosGanador(puntosGanador);
        int expResult = puntosGanador;
        int result = instance.getPuntosGanador();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setPuntosGanador method, of class Sistema.
     */
    @Test
    public void testSetPuntosGanador() {
        
        int puntosGanador = 12;
        Sistema instance = new Sistema();
        instance.setPuntosGanador(puntosGanador);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getPuntosEmpate method, of class Sistema.
     */
    @Test
    public void testGetPuntosEmpate() {
        int puntosEmpate = 3;
        Sistema instance = new Sistema();
        instance.setPuntosEmpate(puntosEmpate);
        int expResult = puntosEmpate;
        int result = instance.getPuntosEmpate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of setPuntosEmpate method, of class Sistema.
     */
    @Test
    public void testSetPuntosEmpate() {
       
        int puntosEmpate = 3;
        Sistema instance = new Sistema();
        instance.setPuntosEmpate(puntosEmpate);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getPuntosResultadoExacto method, of class Sistema.
     */
    @Test
    public void testGetPuntosResultadoExacto() {
        int puntosResultadoExacto = 10;
        Sistema instance = new Sistema();
        instance.setPuntosResultadoExacto(puntosResultadoExacto);
        int expResult = puntosResultadoExacto;
        int result = instance.getPuntosResultadoExacto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of setPuntosResultadoExacto method, of class Sistema.
     */
    @Test
    public void testSetPuntosResultadoExacto() {
        
        int puntosResultadoExacto = 10;
        Sistema instance = new Sistema();
        instance.setPuntosResultadoExacto(puntosResultadoExacto);
        // TODO review the generated test code and remove the default call to fail.
       
    }
    
    @Test
    public void testResultado() {
        
        
        Equipo equipo1 = new Equipo();
        Equipo equipo2 = new Equipo();
        int golE1 = 1;
        int golE2 = 0;
        Sistema instance = new Sistema();
        instance.resultado(equipo1, equipo2, golE1, golE2);
        // TODO review the generated test code and remove the default call to fail.
        
        golE1 = 1;
        golE2 = 1;
        instance.resultado(equipo1, equipo2, golE1, golE2);
        
        golE1 = 0;
        golE2 = 1;
        instance.resultado(equipo1, equipo2, golE1, golE2);
    }

    /**
     * Test of devuelvoEquipo method, of class Sistema.
     */
    @Test
    public void testDevuelvoEquipo() {
        
        Equipo unE = new Equipo();
        unE.setNombre("URUGUAY");
        Equipo dosE = new Equipo();
        dosE.setNombre("FRANCIA");
        Equipo tresE = new Equipo();
        tresE.setNombre("ARGENTINA");
        
        String nombreEquipo = "URUGUAY";
        Sistema instance = new Sistema();
        instance.getListaEquipos()[1] = unE;
        instance.getListaEquipos()[2] = dosE;
        instance.getListaEquipos()[3] = tresE;
        Equipo expResult = unE;
        Equipo result = instance.devuelvoEquipo(nombreEquipo);
       
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of devuelvoJugador method, of class Sistema.
     */
    @Test
    public void testDevuelvoJugador() {
        
        Jugador unJ = new Jugador();
        unJ.setAlias("Diego");
        Jugador dosJ = new Jugador();
        dosJ.setAlias("Alejo");
        Jugador tresJ = new Jugador();;
        tresJ.setAlias("Pepe");
        
        String nombreJugador = "Diego";
        Sistema instance = new Sistema();
        instance.getListaJugadores().add(unJ);
        instance.getListaJugadores().add(dosJ);
        instance.getListaJugadores().add(dosJ);
        Jugador expResult = unJ;
        Jugador result = instance.devuelvoJugador(nombreJugador);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of actualizaRanking method, of class Sistema.
     */
    @Test
    public void testActualizaRanking() {
        
        Sistema instance = new Sistema();
        
        Jugador unJ = new Jugador();
        instance.getListaJugadores().add(unJ);
        Equipo e1 = new Equipo();
       
        Equipo e2 = new Equipo();
       
        Pronostico unP = new Pronostico();
        Partido unPartido = new Partido();
        Partido [] listaPartidos = new Partido [2];
        unPartido.setEquipo1(e1);
        unPartido.setEquipo2(e2);
        unPartido.setGolesEquipo1(2);
        unPartido.setGolesEquipo2(0);
        instance.getListaPartidos()[1]= unPartido;
        
        unP.setEquipo1(e1);
        unP.setEquipo2(e2);
        unP.setGolesEquipo1(2);
        unP.setGolesEquipo2(0);
        unJ.getListaPronostico()[1] = unP;
        
        
       int i = 0;
        
       instance.actualizaRanking(i);
       
       unPartido.setGolesEquipo1(2);
       unPartido.setGolesEquipo2(0);
       instance.getListaPartidos()[1]= unPartido;
       
       unP.setGolesEquipo1(1);
       unP.setGolesEquipo2(0);
        unJ.getListaPronostico()[1] = unP;
        instance.actualizaRanking(i);
        
        instance.actualizaRanking(i);
       
       unPartido.setGolesEquipo1(0);
       unPartido.setGolesEquipo2(2);
       instance.getListaPartidos()[1]= unPartido;
       
       unP.setGolesEquipo1(0);
       unP.setGolesEquipo2(2);
        unJ.getListaPronostico()[1] = unP;
        instance.actualizaRanking(i);
        
        unPartido.setGolesEquipo1(0);
       unPartido.setGolesEquipo2(2);
       instance.getListaPartidos()[1]= unPartido;
       
       unP.setGolesEquipo1(0);
       unP.setGolesEquipo2(1);
        unJ.getListaPronostico()[1] = unP;
        instance.actualizaRanking(i);
        
        unPartido.setGolesEquipo1(2);
       unPartido.setGolesEquipo2(2);
       instance.getListaPartidos()[1]= unPartido;
       
       unP.setGolesEquipo1(2);
       unP.setGolesEquipo2(2);
        unJ.getListaPronostico()[1] = unP;
        instance.actualizaRanking(i);
        
        unPartido.setGolesEquipo1(2);
       unPartido.setGolesEquipo2(2);
       instance.getListaPartidos()[1]= unPartido;
       
       unP.setGolesEquipo1(1);
       unP.setGolesEquipo2(1);
        unJ.getListaPronostico()[1] = unP;
        instance.actualizaRanking(i);
        
        unPartido.setGolesEquipo1(0);
       unPartido.setGolesEquipo2(2);
       instance.getListaPartidos()[49]= unPartido;
       
       unP.setGolesEquipo1(0);
       unP.setGolesEquipo2(2);
        unJ.getListaPronostico()[49] = unP;
        instance.actualizaRanking(i);
        
         unPartido.setGolesEquipo1(2);
       unPartido.setGolesEquipo2(0);
       instance.getListaPartidos()[49]= unPartido;
       
       unP.setGolesEquipo1(2);
       unP.setGolesEquipo2(0);
        unJ.getListaPronostico()[49] = unP;
        instance.actualizaRanking(i);
        
         unPartido.setGolesEquipo1(0);
       unPartido.setGolesEquipo2(2);
       instance.getListaPartidos()[49]= unPartido;
       
       unP.setGolesEquipo1(2);
       unP.setGolesEquipo2(0);
        unJ.getListaPronostico()[49] = unP;
        instance.actualizaRanking(i);
        
         unPartido.setGolesEquipo1(2);
       unPartido.setGolesEquipo2(0);
       instance.getListaPartidos()[49]= unPartido;
       
       unP.setGolesEquipo1(0);
       unP.setGolesEquipo2(2);
        unJ.getListaPronostico()[49] = unP;
        instance.actualizaRanking(i);
        
       
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of existeAlias method, of class Sistema.
     */
    @Test
    public void testExisteAlias() {
        
        Jugador unJ = new Jugador();
        unJ.setAlias("Diego");
        Jugador dosJ = new Jugador();
        dosJ.setAlias("Pepe");
        Sistema instance = new Sistema();
        instance.getListaJugadores().add(unJ);
       
        boolean expResult = true;
        boolean result = instance.existeAlias(unJ.getAlias());
        assertEquals(expResult, result);
        
        expResult = false;
        result = instance.existeAlias(dosJ.getAlias());
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
       
    }

    /**
     * Test of soloNumeros method, of class Sistema.
     */
    @Test
    public void testSoloNumeros() {
        int num = 2;
        
        Sistema instance = new Sistema();
        boolean expResult = true;
        boolean result = instance.soloNumeros(String.valueOf(num));
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
        
        String unNumero = "A";
       
        expResult = false;
        result = instance.soloNumeros(String.valueOf(unNumero));
        assertEquals(expResult, result);
    
    }
   
        
        // TODO review the generated test code and remove the default call to fail.
      
    
    /**
     * Test of devuelveLista method, of class Sistema.
     */
    

    /**
     * Test of devuelveEquipo method, of class Sistema.
     */
    @Test
    public void testDevuelveEquipo() {
        
        Equipo equipo1 = new Equipo();
        equipo1.setPuntos(3);
        Equipo equipo2 = new Equipo();
        equipo2.setPuntos(7);
        Partido unP = new Partido();
        
        int inicio = 1;
        int fin = 2;
        Sistema instance = new Sistema();
        instance.getListaEquipos()[1] = equipo1;
        instance.getListaEquipos()[2] = equipo2;
        Equipo expResult = equipo2;
        Equipo result = instance.devuelveEquipo(inicio, fin);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of devuelveGanador method, of class Sistema.
     */
    @Test
    public void testDevuelveGanador() {
        Equipo equipo1 = new Equipo();
       
        Equipo equipo2 = new Equipo();
       
        Partido unP = new Partido();
        unP.setEquipo1(equipo1);
        unP.setEquipo2(equipo2);
        unP.setGolEquipo1(1);
        unP.setGolEquipo2(3);
        int partido = 1;
        Sistema instance = new Sistema();
        instance.getListaPartidos()[1]= unP;
        Equipo expResult = equipo2;
        
        Equipo result = instance.devuelveGanador(partido);
        assertEquals(expResult, result);
        
        expResult = equipo1;
        unP.setGolEquipo1(3);
        unP.setGolEquipo2(1);
        instance.getListaPartidos()[1]= unP;
        result = instance.devuelveGanador(partido);
        assertEquals(expResult, result);
        
        
        
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of devuelvePerdedor method, of class Sistema.
     */
    @Test
    public void testDevuelvePerdedor() {
        
        Equipo equipo1 = new Equipo();
        Equipo equipo2 = new Equipo();
        Partido unP = new Partido();
        unP.setEquipo1(equipo1);
        unP.setEquipo2(equipo2);
        unP.setGolEquipo1(1);
        unP.setGolEquipo2(3);
        int partido = 1;
        Sistema instance = new Sistema();
        instance.getListaPartidos()[1]=unP;
         Equipo expResult = equipo1;
        Equipo result = instance.devuelvePerdedor(partido);
        assertEquals(expResult, result);
        
         expResult = equipo2;
        unP.setGolEquipo1(3);
        unP.setGolEquipo2(1);
        instance.getListaPartidos()[1]= unP;
        result = instance.devuelvePerdedor(partido);
        assertEquals(expResult, result);
        
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of pronosticoCompleto method, of class Sistema.
     */
   
    /**
     * Test of ganador method, of class Sistema.
     */
    

    /**
     * Test of devuelvoDía method, of class Sistema.
     */
    @Test
    public void testDevuelvoDía() {
        
        int dia = 1;
        Sistema instance = new Sistema();
        String expResult = "Lunes ";
        String result = instance.devuelvoDía(dia);
        assertEquals(expResult, result);
        
        dia = 2;
        expResult = "Martes ";
        result = instance.devuelvoDía(dia);
        assertEquals(expResult, result);
        
        dia = 3;
        expResult = "Miercoles ";
        result = instance.devuelvoDía(dia);
        assertEquals(expResult, result);
        
        dia = 4;
        expResult = "Jueves ";
        result = instance.devuelvoDía(dia);
        assertEquals(expResult, result);
        
        dia = 5;
        expResult = "Viernes ";
        result = instance.devuelvoDía(dia);
        assertEquals(expResult, result);
        
        dia = 6;
        expResult = "Sabado ";
        result = instance.devuelvoDía(dia);
        assertEquals(expResult, result);
        
        dia = 7;
        expResult = "Domingo ";
        result = instance.devuelvoDía(dia);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of devuelvoMes method, of class Sistema.
     */
    @Test
    public void testDevuelvoMes() {
        
        int mes = 1;
        Sistema instance = new Sistema();
        String expResult = "Enero ";
        String result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        
        mes = 2;
        expResult = "Febrero ";
        result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        mes = 3;
        expResult = "Marzo ";
        result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        mes = 4;
        expResult = "Abril ";
        result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        mes = 5;
        expResult = "Mayo ";
        result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        mes = 6;
        expResult = "Junio ";
        result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        mes = 7;
        expResult = "Julio ";
        result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        mes = 8;
        expResult = "Agosto ";
        result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        mes = 9;
        expResult = "Septiembre ";
        result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        mes = 10;
        expResult = "Octubre ";
        result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        mes = 11;
        expResult = "Noviembre ";
        result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        mes = 12;
        expResult = "Diciembre ";
        result = instance.devuelvoMes(mes);
        assertEquals(expResult, result);
        
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of fechaValida method, of class Sistema.
     */
    @Test
    public void testFechaValida() {
       
        Partido unP = new Partido();
        Calendar calendario = new GregorianCalendar();
          int mes = calendario.get(Calendar.MONTH) + 1;
          int dia = calendario.get(Calendar.DAY_OF_MONTH);
          int hora = calendario.get(Calendar.HOUR_OF_DAY);
        unP.setMes(8);
       
        int posicion = 1;
        
        Sistema instance = new Sistema();
        instance.getListaPartidos()[posicion]= unP;
        boolean expResult = true;
        boolean result = instance.fechaValida(posicion);
        assertEquals(expResult, result);
        
       
        unP.setMes(6);
        unP.setDia(30);
        instance.getListaPartidos()[posicion]= unP;
        expResult = true;
        result = instance.fechaValida(posicion);
        assertEquals(expResult, result);
        
        unP.setMes(5);
        
        instance.getListaPartidos()[posicion]= unP;
        expResult = false;
        result = instance.fechaValida(posicion);
        assertEquals(expResult, result);
        
        // se setea de esta manera porque no sabemos en que momento Dia/hora se corregirá la prueba.
        unP.setMes(mes);
        unP.setDia(dia);
        unP.setHora(hora + 1);
        instance.getListaPartidos()[posicion]= unP;
        expResult = true;
        result = instance.fechaValida(posicion);
        assertEquals(expResult, result);
        
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
}
