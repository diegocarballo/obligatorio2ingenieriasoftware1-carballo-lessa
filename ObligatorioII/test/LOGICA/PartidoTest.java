/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Diego
 */
public class PartidoTest {
    
    public PartidoTest() {
        Equipo unE = new Equipo();
        Equipo dosE = new Equipo();
        Partido unP = new Partido(unE,dosE, 2,2, "Moscu Arenas", "Jueves", 6, 15, 12, 25);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEquipo1 method, of class Partido.
     */
    @Test
    public void testGetEquipo1() {
        
        Equipo e1 = new Equipo();
        Partido instance = new Partido();
        Equipo expResult = e1;
        instance.setEquipo1(e1);
        Equipo result = instance.getEquipo1();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setEquipo1 method, of class Partido.
     */
    @Test
    public void testSetEquipo1() {
       
         Equipo e1 = new Equipo();
        Partido instance = new Partido();
        instance.setEquipo1(e1);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getEquipo2 method, of class Partido.
     */
    @Test
    public void testGetEquipo2() {
        Equipo e2 = new Equipo();
        Partido instance = new Partido();
        Equipo expResult = e2;
        instance.setEquipo2(e2);
        Equipo result = instance.getEquipo2();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setEquipo2 method, of class Partido.
     */
    @Test
    public void testSetEquipo2() {
       Equipo e2 = new Equipo();
        Partido instance = new Partido();
        instance.setEquipo2(e2);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of getGolEquipo1 method, of class Partido.
     */
    @Test
    public void testGetGolEquipo1() {
        
        Partido instance = new Partido();
        
        int expResult = 2;
        instance.setGolEquipo1(expResult);
        int result = instance.getGolEquipo1();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of setGolEquipo1 method, of class Partido.
     */
    @Test
    public void testSetGolEquipo1() {
       
        int golEquipo1 = 0;
        Partido instance = new Partido();
       
        instance.setGolEquipo1(golEquipo1);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getGolEquipo2 method, of class Partido.
     */
    @Test
    public void testGetGolEquipo2() {
       
        Partido instance = new Partido();
        int expResult = 2;
        instance.setGolEquipo2(expResult);
        int result = instance.getGolEquipo2();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    
    }

    /**
     * Test of setGolEquipo2 method, of class Partido.
     */
    @Test
    public void testSetGolEquipo2() {
        
        int golEquipo2 = 0;
        Partido instance = new Partido();
        instance.setGolEquipo2(golEquipo2);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getGolesEquipo1 method, of class Partido.
     */
    @Test
    public void testGetGolesEquipo1() {
       
        Partido instance = new Partido();
        int expResult = 0;
        instance.setGolEquipo1(expResult);
        int result = instance.getGolesEquipo1();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setGolesEquipo1 method, of class Partido.
     */
    @Test
    public void testSetGolesEquipo1() {
       
        int golesEquipo1 = 3;
        Partido instance = new Partido();
        instance.setGolesEquipo1(golesEquipo1);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getGolesEquipo2 method, of class Partido.
     */
    @Test
    public void testGetGolesEquipo2() {
        
        Partido instance = new Partido();
        int expResult = 2;
        instance.setGolEquipo2(expResult);
        int result = instance.getGolesEquipo2();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setGolesEquipo2 method, of class Partido.
     */
    @Test
    public void testSetGolesEquipo2() {
        
        int golesEquipo2 = 1;
        Partido instance = new Partido();
        instance.setGolesEquipo2(golesEquipo2);
        // TODO review the generated test code and remove the default call to fail.
     
    }

    /**
     * Test of getEstadio method, of class Partido.
     */
    @Test
    public void testGetEstadio() {
        
        Partido instance = new Partido();
        String expResult = "Moscu  Arenas";
        instance.setEstadio(expResult);
        String result = instance.getEstadio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of setEstadio method, of class Partido.
     */
    @Test
    public void testSetEstadio() {
        
        String estadio = "Moscu Arenas";
        Partido instance = new Partido();
        instance.setEstadio(estadio);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of getFechaHora method, of class Partido.
     */
    @Test
    public void testGetFechaHora() {
       
        Partido instance = new Partido();
        String expResult = "9 Julio, 15:00 horas";
        instance.setInfo(expResult);
        String result = instance.getInfo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setFechaHora method, of class Partido.
     */
    @Test
    public void testSetFechaHora() {
       
        String fechaHora = "9 Julio, 15:00 horas";
        Partido instance = new Partido();
        instance.setInfo(fechaHora);
        // TODO review the generated test code and remove the default call to fail.
       
    }
    @Test
    public void testGetMinutos() {
        Partido instance = new Partido();
        int expResult = 5;
        instance.setMinutos(expResult);
        int result = instance.getMinutos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }
}
