/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Diego
 */
public class PronosticoTest {
    
    public PronosticoTest() {
        
         Pronostico unP = new Pronostico (3, 5);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEquipo1 method, of class Pronostico.
     */
    @Test
    public void testGetEquipo1() {
        
        Equipo unE = new Equipo();
        Pronostico instance = new Pronostico();
        instance.setEquipo1(unE);
        Equipo expResult = unE;
        Equipo result = instance.getEquipo1();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setEquipo1 method, of class Pronostico.
     */
    @Test
    public void testSetEquipo1() {
        
        Equipo unE = new Equipo();
        Pronostico instance = new Pronostico();
        instance.setEquipo1(unE);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getEquipo2 method, of class Pronostico.
     */
    @Test
    public void testGetEquipo2() {
        
        Equipo unE = new Equipo();
        Pronostico instance = new Pronostico();
        Equipo expResult = unE;
        instance.setEquipo2(unE);
        Equipo result = instance.getEquipo2();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of setEquipo2 method, of class Pronostico.
     */
    @Test
    public void testSetEquipo2() {
        Equipo unE = new Equipo();
        Pronostico instance = new Pronostico();
        instance.setEquipo2(unE);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getGolesEquipo1 method, of class Pronostico.
     */
    @Test
    public void testGetGolesEquipo1() {
       
        Pronostico instance = new Pronostico();
        instance.setGolesEquipo1(5);
        int expResult = 5;
        int result = instance.getGolesEquipo1();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setGolesEquipo1 method, of class Pronostico.
     */
    @Test
    public void testSetGolesEquipo1() {
        
        
        int golesEquipo1 = 5;
        Pronostico instance = new Pronostico();
        instance.setGolesEquipo1(golesEquipo1);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getGolesEquipo2 method, of class Pronostico.
     */
    @Test
    public void testGetGolesEquipo2() {
        
        Pronostico instance = new Pronostico();
        instance.setGolesEquipo2(5);
        int expResult = 5;
        int result = instance.getGolesEquipo2();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of setGolesEquipo2 method, of class Pronostico.
     */
    @Test
    public void testSetGolesEquipo2() {
         int golesEquipo2 = 5;
        Pronostico instance = new Pronostico();
        instance.setGolesEquipo2(golesEquipo2);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
}
