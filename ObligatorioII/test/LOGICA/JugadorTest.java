/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Diego
 */
public class JugadorTest {
    
    public JugadorTest() {
        
        Jugador unJ = new Jugador ("Diego", 25);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAlias method, of class Jugador.
     */
    @Test
    public void testGetAlias() {
        
        Jugador instance = new Jugador();
        String expResult = "DIEGO";
        instance.setAlias(expResult);
        String result = instance.getAlias();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of setAlias method, of class Jugador.
     */
    @Test
    public void testSetAlias() {
        
        String alias = "";
        Jugador instance = new Jugador();
        instance.setAlias(alias);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getPuntos method, of class Jugador.
     */
    @Test
    public void testGetPuntos() {
        
        Jugador instance = new Jugador();
        int expResult = 3;
        instance.setPuntos(expResult);
        int result = instance.getPuntos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setPuntos method, of class Jugador.
     */
    @Test
    public void testSetPuntos() {
        
        int puntos = 0;
        Jugador instance = new Jugador();
        instance.setPuntos(puntos);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of getListaPronostico method, of class Jugador.
     */
    @Test
    public void testGetListaPronostico() {
        
        Pronostico [] listaPronostico = new Pronostico [65];
        Jugador instance = new Jugador();
        instance.setListaPronostico(listaPronostico);
        Pronostico[] expResult = listaPronostico;
        Pronostico[] result = instance.getListaPronostico();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setListaPronostico method, of class Jugador.
     */
    @Test
    public void testSetListaPronostico() {
        System.out.println("setListaPronostico");
        Pronostico[] listaPronostico = null;
        Jugador instance = new Jugador();
        instance.setListaPronostico(listaPronostico);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of agregarPronostico method, of class Jugador.
     */
    @Test
    public void testAgregarPronostico() {
        
        
        Pronostico unP = new Pronostico();
        
        int posicion = 1;
        Jugador instance = new Jugador();
        instance.getListaPronostico()[1] = unP;
        instance.agregarPronostico(unP, posicion);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of toString method, of class Jugador.
     */
    @Test
    public void testToString() {
       
        Jugador instance = new Jugador();
        
        String expResult = instance.toString();
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }
    
}
